import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

import com.almworks.sqlite4java.SQLiteException;

import ua.org.unixander.graph.ExtendedEdge;
import ua.org.unixander.graph.Graph;
import ua.org.unixander.graph.Node;
import ua.org.unixander.graph.algo.AStarAlgorithm;
import ua.org.unixander.graph.algo.AntColonyAlgorithm;
import ua.org.unixander.graph.algo.BranchAndBound;
import ua.org.unixander.graph.algo.Sequential2OptRunner;
import ua.org.unixander.graph.extendedalgo.AStarAlgorithmExt;
import ua.org.unixander.graph.extendedalgo.AntColonyAlgorithmExt;
import ua.org.unixander.graph.extendedalgo.BranchAndBoundExt;
import ua.org.unixander.graph.extendedalgo.applier.*;
import ua.org.unixander.graph.fuzzy.FuzzyEdge;
import ua.org.unixander.graph.fuzzy.FuzzyGraph;
import ua.org.unixander.graph.fuzzy.FuzzyGraphBuilder;
import ua.org.unixander.graph.utils.GraphBuilder;
import ua.org.unixander.graph.utils.SQLiteConnector;

public class MainClass {

	public static void main(String[] args) throws SQLiteException {
		MainClass.testAntColony();
		//MainClass.testAntColony();
		// MainClass.test();
	}

	public static void SampleGraph() {
		Graph graph = new Graph();
		for (int i = 1; i < 5; i++)
			graph.addNode(new Node(i));

		ExtendedEdge edge = new ExtendedEdge(new Node(1), new Node(2),
				new Node(3), new Node(4));
		System.out.println(edge.toString());
		graph.addEdge(1, 2);
		graph.addEdge(1, 3);
		graph.addEdge(3, 4);

	}

	public static void test() {
		FuzzyGraph graph = new FuzzyGraph();
		for (int i = 1; i < 5; i++)
			graph.addNode(new Node(i));
		
		double[] ins = new double[]{1,2};
		
		FuzzyEdge edge = new FuzzyEdge(new Node(1), new Node(2));
		edge.setINs(ins);
		graph.addEdge(edge);
		graph.findEdge(new Node(1), new Node(2));
		graph.getFuzzyEdgeValue(new Node(1), new Node(2), 1);
	}

	public static void SampleGraphBuilder() {
		GraphBuilder gb = new GraphBuilder();
		gb.loadGraphFromDB();
		write(gb.getGraph());
		Graph graph = gb.getGraph();
		AStarAlgorithm.setApplier(new HighwayApply(graph));
		Scanner scanner = new Scanner(System.in);
		while(true){
			long srcId = 280944891, trgtId = 2112074110;
			srcId = scanner.nextLong();
			trgtId = scanner.nextLong();
			Node source = graph.findNode(srcId), target = graph.findNode(trgtId);
			List<Node> path = AStarAlgorithm.search(graph, source, target);
			if (path == null) {
				System.out.println("Path not found");
			} else {
				System.out.println(path.toString());
			}
		}
	}
	
	public static void SampleGraphBuilderExt() {
		FuzzyGraphBuilder gb = new FuzzyGraphBuilder();
		gb.loadGraphFromDB();
		write(gb.getGraph());
		FuzzyGraph graph = gb.getGraph();
		long srcId = 1802881136, trgtId = 834544548;
		Node source = graph.findNode(srcId), target = graph.findNode(trgtId);
		if(source==null || target == null){
			System.out.println("Not found node");
		}
		AStarAlgorithmExt.setApplier(new AllApply());
		List<Node> path = AStarAlgorithmExt.search(graph, source, target);
		if (path == null) {
			System.out.println("Path not found");
		} else {
			for(int i = 1; i < path.size(); i++) {
				FuzzyEdge edge = (FuzzyEdge) graph.findEdge(path.get(i-1), path.get(i));
				System.out.println(Arrays.toString(edge.getINs()));
			}
			System.out.println(path.toString());
		}
	}

	public static void testAntColony() {

		long start = System.currentTimeMillis();
		double[][] matrix = { { 0, 20, 20, 16, 13, 16, 20, 11, 21, 31 },
				{ 18, 0, 20, 16, 12, 13, 18, 12, 22, 32 },
				{ 16, 14, 0, 16, 0, 21, 14, 13, 23, 33 },
				{ 16, 9, 14, 0, 9, 19, 22, 14, 24, 34 },
				{ 0, 12, 19, 20, 0, 21, 12, 15, 25, 35 },
				{ 11, 11, 22, 22, 14, 0, 18, 16, 26, 36 },
				{ 12, 12, 9, 0, 16, 15, 0, 17, 27, 37 },
				{ 10, 13, 8, 23, 13, 13, 12, 0, 11, 12 },
				{ 11, 22, 21, 24, 26, 18, 18, 13, 0, 13 },
				{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 } };
		AntColonyAlgorithm algo;
		AntColonyAlgorithmExt algoe;
		BranchAndBound bnb;
		try {
			for (int i = 5; i < 8; i++) {
				for (int j = 0; j < 1; j++) {
					matrix = generateMatrix(i);
					int st = 0, finish = i - 1;
					System.out.println("Size: " + i + " x " + i);
					start = System.currentTimeMillis();
					algo = new AntColonyAlgorithm(matrix, st, finish);
					double distance = algo.calculate();
					System.out.println("Ant: " + distance + " | "
							+ (System.currentTimeMillis() - start));
					System.out.println(Arrays.toString(algo.getBestWay()));
					/*
					 * System.out.println("Best distance ant: " + distance + " "
					 * + Arrays.toString(algo.getBestWay()) + "" + " Time: " +
					 * (System.currentTimeMillis() - start));
					 */
					
					start = System.currentTimeMillis();
					algoe = new AntColonyAlgorithmExt(matrix, st, finish);
					distance = algoe.calculate();
					System.out.println("AntExt: " + distance + " | "
							+ (System.currentTimeMillis() - start));
					
					start = System.currentTimeMillis();
					bnb = new BranchAndBound();
					bnb.calculate(matrix);
					distance = bnb.getDistance();
					System.out.println("Bnb: " + distance + " | "
							+ (System.currentTimeMillis() - start));
					/*
					 * System.out.println("Best distance BnB: " +
					 * bnb.getDistance() + " way: " +
					 * Arrays.toString(bnb.getBestWay()) + " Time: " +
					 * (System.currentTimeMillis() - start));
					 */
					
					
					
				}
			}
		} catch (IOException | InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
	}

	public static double[][] generateMatrix(int N) {
		double[][] result = new double[N][N];
		Random rand = new Random();
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				if (i == j)
					result[i][j] = 0;
				else
					result[i][j] = rand.nextDouble() * 20;
			}
		}
		return result;
	}

	public static void write(Graph graph) {
		String path = "";
		try {
			path = new File(".").getCanonicalPath();

			File file = new File(path + "graph.txt");
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(graph.toString());
			bw.close();
			System.out.println("File done!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
