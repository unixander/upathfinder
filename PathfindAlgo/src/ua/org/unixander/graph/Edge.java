package ua.org.unixander.graph;

import java.util.HashMap;
import java.util.Map;

public class Edge {
	protected Node source, destination;
	protected double weight;
	protected Map<String, String> tags;

	public Edge(Node source, Node destination) {
		this(source, destination, new HashMap<String, String>(), 0);
	}

	public Edge(Node source, Node destination, double weight) {
		this(source, destination, new HashMap<String, String>(), weight);
	}
	
	public Edge(Node source, Node destination, HashMap<String, String> tags,
			double weight) {
		this.source = source;
		this.destination = destination;
		this.weight = weight;
		this.tags = tags;
	}

	public Node getSource() {
		return source;
	}

	public Node getDestination() {
		return destination;
	}

	public double getWeight() {
		return weight;
	}

	public void setSource(Node source) {
		this.source = source;
	}

	public void setDestination(Node destination) {
		this.destination = destination;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public void setTags(Map<String, String> tags) {
		this.tags.putAll(tags);
	}

	public Map<String, String> getTags() {
		return this.tags;
	}

	public void addTag(String key, String value) {
		this.tags.put(key, value);
	}

	public String removeTag(String key) {
		return this.tags.remove(key);
	}

	public String getTag(String key) {
		return this.tags.get(key);
	}

	public boolean checkTagKey(String key) {
		return this.tags.containsKey(key);
	}

	public boolean checkTagValue(String value) {
		return this.tags.containsValue(value);
	}


	@Override
	public String toString() {
		return source + " -> " + destination;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		//if (getClass() != obj.getClass())
		//	return false;
		Edge other = (Edge) obj;
		if (this.source.equals(other.source)
				&& this.destination.equals(other.destination)){
			return true;
		}
		return false;
	}
}
