package ua.org.unixander.graph.fuzzy;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;

import ua.org.unixander.graph.Edge;
import ua.org.unixander.graph.Node;
import ua.org.unixander.graph.utils.FuzzyCalculations;
import ua.org.unixander.graph.utils.SQLiteConnector;

public class FuzzyGraphBuilder {
	private FuzzyGraph graph;
	private SQLiteConnector db;
	private LinkedList<Node> nodes;
	private LinkedList<Node> processed;
	private long startId = 1326181881;//1280287429; // 1067363212;
	private FuzzyCalculations fuzzy;
	private Random rnd = new Random();
	
	public FuzzyGraphBuilder() {
		this.db = new SQLiteConnector();
		nodes = new LinkedList<Node>();
		processed = new LinkedList<Node>();
		graph = new FuzzyGraph();
		String path = "";
		try {
			path = new File(".").getCanonicalPath();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		fuzzy = new FuzzyCalculations(path);
	}

	public void loadGraphFromDB() {
		Node temp = null;
		ArrayList<Node> nbrs = null;
		try {
			db.open();
			temp = db.getNodeById(startId);
			graph.addNode(temp);
			nodes.add(temp);
			while(!nodes.isEmpty()){
				temp = nodes.remove();
				processed.add(temp);
				nbrs = db.getNeighboursByNodeId(temp.getId());
				if (nbrs != null) {
					for (Node n : nbrs) {
						graph.addNode(n);
						FuzzyEdge edge = new FuzzyEdge(temp, n);
						Map<String, String> tags = getWayTagsForEdge(temp, n);
						
						double[] ins = getIns(edge);
						edge.setINs(ins);
						double[][] autoFuzzy = fuzzy.calculateFuzzyValue(ins);
						edge.setAutoFuzzy(autoFuzzy);
						
						if(tags!=null && !tags.isEmpty()){
							edge.setTags(tags);
						}
						graph.addEdge(edge);
						if (!graph.isOneDirection()) {
							edge = new FuzzyEdge(n, temp);
							if(tags!=null && !tags.isEmpty()){
								edge.setTags(tags);
							}
							edge.setAutoFuzzy(autoFuzzy);
							edge.setINs(ins);
							graph.addEdge(edge);
						}
						if(processed.indexOf(n) == -1 && nodes.indexOf(n) == -1){ 
							nodes.add(n);
						}
					}
					System.out.println(graph.getNodes().size()+":::"+graph.getEdges().size()+"\r\n");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			db.close();
		}
	}

	public Map<String, String> getWayTagsForEdge(Node src, Node dest){
		HashMap<String, String> result = new HashMap<String, String>();
		ArrayList<Long> ways1, ways2;
		ways1 =  db.getWaysByNodeId(src.getId());
		ways2 = db.getWaysByNodeId(dest.getId());
		ArrayList<Long> common = new ArrayList<Long>();
		for(Long way1: ways1){
			if(ways2.indexOf(way1)>-1){
				common.add(way1);
				result.putAll(db.getTagsByWayId(way1));
			}
		}
		return result;
	}

	public FuzzyGraph getGraph() {
		return this.graph;
	}
	
	public double[] getIns(FuzzyEdge edge){
		double[] ins = new double[2];
		ins[0] = rnd.nextInt(20) * 0.05;
		ins[1] = rnd.nextInt(20) * 0.05;
		return ins;
	}

}
