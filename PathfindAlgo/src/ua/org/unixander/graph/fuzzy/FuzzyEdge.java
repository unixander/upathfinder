package ua.org.unixander.graph.fuzzy;

import cern.colt.Arrays;
import ua.org.unixander.graph.Edge;
import ua.org.unixander.graph.Node;

public class FuzzyEdge extends Edge {
	
	private double[][] autoFuzzy, transitFuzzy;
	private double[] ins;
	
	public FuzzyEdge(Node source, Node destination) {
		super(source, destination);
	}

	public double[][] getAutoFuzzy() {
		return autoFuzzy;
	}

	public void setAutoFuzzy(double[][] autoFuzzy) {
		this.autoFuzzy = autoFuzzy;
	}

	public double[][] getTransitFuzzy() {
		return transitFuzzy;
	}

	public void setTransitFuzzy(double[][] transitFuzzy) {
		this.transitFuzzy = transitFuzzy;
	}
	
	public void setINs(double[] ins) {
		this.ins = ins;
	}
	
	public double[] getINs(){
		return this.ins;
	}
	
	@Override
	public String toString() {
		return super.toString() + " Fuzzy: " + Arrays.toString(this.ins);
	}
	
}
