package ua.org.unixander.graph.fuzzy;

import ua.org.unixander.graph.Graph;
import ua.org.unixander.graph.Node;

public class FuzzyGraph extends Graph {
	public static final int AUTO = 1, TRANSIT = 2;
	
	public double[][] getFuzzyEdgeValue(Node source, Node destination, int type) {
		double[][] result = null;
		FuzzyEdge edge = (FuzzyEdge) this.findEdge(source, destination);
		if (edge == null)
			return null;
		switch (type) {
		case TRANSIT:
			result = edge.getTransitFuzzy();
			break;
		case AUTO:
			result = edge.getAutoFuzzy();
		}
		return result;
	}
	
}
