package ua.org.unixander.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Graph {
	private List<Node> nodes = new ArrayList<Node>();
	private List<Edge> edges = new ArrayList<Edge>();
	private Map<Node, HashSet<Node>> adjacencies = new HashMap<Node, HashSet<Node>>();
	private boolean oneDirection = false;

	public Graph(List<Node> nodes, List<Edge> edges) {
		this.nodes = nodes;
		this.edges = edges;
	}

	public Graph() {

	}

	public List<Node> getNodes() {
		return this.nodes;
	}

	public List<Edge> getEdges() {
		return this.edges;
	}

	public Set<Node> getAdjacency(Node node) {
		return adjacencies.get(node);
	}

	public void addNode(Node node) {
		if (!this.nodes.contains(node)) {
			this.nodes.add(node);
			if (!this.adjacencies.containsKey(node))
				this.adjacencies.put(node, new HashSet<Node>());
		}
	}

	public Node findNode(Node node) {
		int index = this.nodes.indexOf(node);
		if (index > -1)
			return this.nodes.get(index);
		else
			return null;
	}

	public Node findNode(long id) {
		return findNode(new Node(id));
	}
	
	public Edge findEdge(Node src, Node dest){
		Edge temp = new Edge(src,dest);
		int index = this.edges.indexOf(temp);
		if(index < 0){
			return null;
		} else {
			return this.edges.get(index);
		}
	}
	
	public void addEdge(Edge edge) {
		if (!this.edges.contains(edge)) {
			this.edges.add(edge);
			HashSet<Node> adjs = this.adjacencies.get(edge.source);
			adjs.add(edge.destination);
			this.adjacencies.put(edge.source, adjs);
		} else {
			int i = this.edges.indexOf(edge);
			this.edges.set(i, edge);
		}
	}

	public void addEdge(Node source, Node destination) {
		if (nodes.indexOf(source) == -1)
			nodes.add(source);
		if (nodes.indexOf(destination) == -1)
			nodes.add(destination);
		Edge edge = new Edge(source, destination, this.calcDistance(source,
				destination));
		if (!this.edges.contains(edge)) {
			this.edges.add(edge);
			HashSet<Node> adjs = this.adjacencies.get(source);
			adjs.add(destination);
			this.adjacencies.put(source, adjs);
		} else {
			// System.out.println("Edge " + edge.toString() +
			// " already presents");
		}
	}

	public void addEdge(long sourceId, long destId) {
		int s = nodes.indexOf(new Node(sourceId)), d = nodes.indexOf(new Node(
				destId));
		if (s < 0) {
			nodes.add(new Node(sourceId));
			s = nodes.indexOf(new Node(sourceId));
		}
		if (d < 0) {
			nodes.add(new Node(destId));
			d = nodes.indexOf(new Node(destId));
		}
		Node src = nodes.get(s), dest = nodes.get(d);
		this.addEdge(src, dest);
		if (!oneDirection)
			this.addEdge(dest, src);
	}

	public double calcManhattanDistance(Node source, Node destination) {
		if (source.getPosition() != null && destination.getPosition() != null) {
			return Math.abs(source.getPosition().getLatitude()
					- destination.getPosition().getLatitude())
					+ Math.abs(source.getPosition().getLongitude()
							- destination.getPosition().getLongitude());
		} else
			return 0;
	}
	
	public double calcDistance(Node source, Node destination) {
		double result = 0, x1, x2, y1, y2;
		double distance = 0;
		double R = 6371;//69.09 * 1.6093;
		if (source.getPosition() != null && destination.getPosition() != null) {
			x1 = source.getPosition().getLatitude();
			x2 = destination.getPosition().getLatitude();
			y1 = source.getPosition().getLongitude();
			y2 = destination.getPosition().getLongitude();
			distance = (Math.sin(Math.toRadians(x1))
					* Math.sin(Math.toRadians(x2)) + Math.cos(Math
					.toRadians(x1))
					* Math.cos(Math.toRadians(x2))
					* Math.cos(Math.toRadians(y1 - y2)));
			distance = (Math.toDegrees(Math.acos(distance))) * R;
			// System.out.println("Distance: "+distance+" km");
			result = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
		}
		return result;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		/*
		 * sb.append("Graph:\r\nNodes:\r\n"); for (Node n : this.nodes) {
		 * sb.append(n.toString() + "\r\n"); } sb.append("\r\n\r\nEdges:\r\n");
		 * for (Edge e : this.edges) { sb.append(e.toString() + "\r\n"); }
		 */
		sb.append("\r\n\r\nAdjacencies:\r\n");
		for (Entry<Node, HashSet<Node>> entry : adjacencies.entrySet()) {
			Node key = entry.getKey();
			HashSet<Node> value = entry.getValue();
			sb.append(key.toString() + ": ");
			for (Node n : value) {
				sb.append(n.toString() + " ");
			}
			sb.append("\r\n");
		}
		return sb.toString();
	}

	public boolean isOneDirection() {
		return oneDirection;
	}
}
