package ua.org.unixander.graph.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import ua.org.unixander.graph.Node;

import com.almworks.sqlite4java.SQLiteConnection;
import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteStatement;

public class SQLiteConnector {
	private SQLiteConnection db;
	public static String DB_PATH = "\\db\\routes.sqlite";
	private File dbFile;
	private static boolean DEBUG = false;

	public SQLiteConnector() {
		if (checkDbFileExist()) {
			db = new SQLiteConnection(dbFile);
		}
	}

	private boolean checkDbFileExist() {
		String path = "";
		try {
			path = new File(".").getCanonicalPath();
		} catch (IOException e) {
			e.printStackTrace();
		}
		dbFile = new File(path + DB_PATH);
		if (!dbFile.exists()) {
			System.out.println("Database file not found");
			dbFile = null;
			return false;
		}
		return true;
	}

	public void open() throws SQLiteException {
		if (db != null && !db.isOpen())
			db.openReadonly();
	}

	public ArrayList<long[]> getWaysAndOrdersByNodeId(long id) {
		SQLiteStatement st = null;
		String sql = "SELECT way_id, local_order FROM way_nodes WHERE way_nodes.node_id = ?";
		ArrayList<long[]> result = new ArrayList<long[]>();
		try {
			st = db.prepare(sql);
			st.bind(1, id);
			long[] temp;
			if (DEBUG) {
				System.out.print("way_id|order: ");
			}
			while (st.step()) {
				temp = new long[2];
				temp[0] = st.columnLong(0);
				temp[1] = st.columnLong(1);
				result.add(temp);
				if (DEBUG) {
					System.out.print(temp[0] + "|" + temp[1] + "; ");
				}
			}
			if (DEBUG) {
				System.out.println();
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				st.dispose();
		}
		return result;
	}
	
	public ArrayList<Long> getWaysByNodeId(long id) {
		SQLiteStatement st = null;
		String sql = "SELECT way_id FROM way_nodes WHERE way_nodes.node_id = ?";
		ArrayList<Long> result = new ArrayList<Long>();
		try {
			st = db.prepare(sql);
			st.bind(1, id);
			while (st.step()) {
				result.add(st.columnLong(0));
			}
			if (DEBUG) {
				System.out.println();
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				st.dispose();
		}
		return result;
	}

	public Node getNodeById(long id) {
		SQLiteStatement st = null;
		String sql = "SELECT id, lat, lon FROM nodes WHERE id = ?";
		Node node = null;
		Map<String, String> tags, rel_tags, node_tags, stop_tags;
		try {
			st = db.prepare(sql);
			st.bind(1, id);
			if (st.step()) {
				node = new Node(st.columnLong(0));
				node.setPosition(st.columnDouble(2), st.columnDouble(1));
				tags = node.getTags();
				node_tags = getWayTagsByNodeId(id);
				if(node_tags !=null && !node_tags.isEmpty()){
					tags.putAll(node_tags);
				}
				rel_tags = getRefRelationTagsByNodeId(id);
				if (rel_tags != null && !rel_tags.isEmpty()) {
					tags.putAll(rel_tags);
				}
				node_tags = getTagsByNodeId(id);
				if (node_tags != null && !node_tags.isEmpty()) {
					tags.putAll(node_tags);
				}
				if (tags.containsKey("bus") && !tags.containsKey("name")) {
					stop_tags = getBusStopTags(node);
					if (stop_tags != null && !stop_tags.isEmpty()) {
						tags.putAll(stop_tags);
					}
				}
				//System.out.println(tags.toString());
				node.setTags(tags);
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				st.dispose();
		}
		return node;
	}

	public Map<String, String> getBusStopTags(Node node) {
		Map<String, String> tags = new HashMap<String, String>();
		SQLiteStatement st = null;
		String sql = "SELECT nodes.id, nodes.lat, nodes.lon "
				+ "FROM nodes , node_tags WHERE node_tags.key = 'highway' AND node_tags.value = 'bus_stop' AND "
				+ "nodes.id = node_tags.node_id AND abs(nodes.lat-?)<0.001 AND abs(nodes.lon-?)<0.001";
		String key, value;
		double distance = Double.MAX_VALUE, temp;
		double lon, lat, nLat = node.getPosition().getLatitude(), nLon = node
				.getPosition().getLongitude();
		long id = -1;
		try {
			st = db.prepare(sql);
			st.bind(1, nLat);
			st.bind(2, nLon);
			while (st.step()) {
				lat = st.columnDouble(1);
				lon = st.columnDouble(2);
				temp = calcDistance(lat, lon, nLat, nLon);
				if (temp < distance) {
					distance = temp;
					id = st.columnLong(0);
				}
			}
			st.dispose();
			st = db.prepare("SELECT key, value FROM node_tags WHERE node_tags.node_id=?");
			st.bind(1, id);
			while (st.step()) {
				key = st.columnString(0);
				value = st.columnString(1);
				tags.put(key, value);
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				st.dispose();
		}
		return tags;
	}

	private double calcDistance(double lat1, double lon1, double lat2,
			double lon2) {
		return Math.sqrt(Math.pow((lat2 - lat1), 2)
				+ Math.pow((lon2 - lon1), 2));
	}

	public Map<String, String> getTagsByNodeId(long id) {
		SQLiteStatement st = null;
		String sql = "SELECT key, value FROM node_tags WHERE node_tags.node_id = ?";
		Map<String, String> tags = new HashMap<String, String>();
		try {
			st = db.prepare(sql);
			st.bind(1, id);
			while (st.step()) {
				tags.put(st.columnString(0), st.columnString(1));
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				st.dispose();
		}
		return tags;
	}

	public Map<String, String> getTagsByWayId(long id) {
		SQLiteStatement st = null;
		String sql = "SELECT key, value FROM way_tags WHERE way_tags.way_id = ?";
		Map<String, String> tags = new HashMap<String, String>();
		try {
			st = db.prepare(sql);
			st.bind(1, id);
			while (st.step()) {
				tags.put(st.columnString(0), st.columnString(1));
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				st.dispose();
		}
		return tags;
	}

	public HashSet<String> getTagValueByNodeId(long id, String key) {
		SQLiteStatement st = null;
		String[] sql = new String[] {
				"SELECT way_tags.value FROM "
						+ "way_nodes, way_tags WHERE way_tags.key = ? AND way_tags.way_id = way_nodes.way_id AND way_nodes.node_id = ?",
				"SELECT  relation_tags.value FROM "
						+ "way_nodes, relation_members, relation_tags WHERE relation_tags.key = ? AND way_nodes.node_id = ? "
						+ "AND relation_members.ref = way_nodes.way_id AND "
						+ "relation_tags.relation_id = relation_members.relation_id"
						+ " GROUP BY relation_tags.value ORDER BY "
						+ "relation_tags.relation_id ASC",
				"SELECT value FROM node_tags WHERE key = ? AND node_tags.node_id = ?" };
		HashSet<String> result = new HashSet<String>();
		for (int i = 0; i < 3; i++) {
			try {
				st = db.prepare(sql[i]);
				st.bind(1, key);
				st.bind(2, id);
				while(st.step()){
					result.add(st.columnString(0));
				}
			} catch (SQLiteException e){
				e.printStackTrace();
			}
		}
		return result;
	}

	public Map<String, String> getWayTagsByNodeId(long id) {
		SQLiteStatement st = null;
		String sql = "SELECT way_tags.key, way_tags.value FROM "
				+ "way_nodes, way_tags WHERE way_tags.way_id = way_nodes.way_id AND way_nodes.node_id = ?";
		Map<String, String> tags = new HashMap<String, String>();
		String key, value;
		try {
			st = db.prepare(sql);
			st.bind(1, id);
			while (st.step()) {
				key = st.columnString(0);
				value = st.columnString(1);
				if (tags.containsKey(key)) {
					tags.put(key, tags.get(key) + ";" + value);
				} else {
					tags.put(key, value);
				}
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				st.dispose();
		}
		return tags;
	}

	public Map<String, String> getRefRelationTagsByNodeId(long id) {
		SQLiteStatement st = null;
		Map<String, String> tags = new HashMap<String, String>();
		String sql = "SELECT relation_tags.key, relation_tags.value FROM "
				+ "way_nodes, relation_members, relation_tags WHERE way_nodes.node_id = ? "
				+ "AND relation_members.ref = way_nodes.way_id AND "
				+ "relation_tags.relation_id = relation_members.relation_id AND "
				+ "relation_tags.key='ref' GROUP BY relation_tags.value ORDER BY "
				+ "relation_tags.relation_id ASC";
		String key = null, value = null;
		try {
			st = db.prepare(sql);
			st.bind(1, id);
			while (st.step()) {
				key = st.columnString(0);
				value = st.columnString(1);
				if (tags.containsKey(key)) {
					tags.put(key, tags.get(key) + ";" + value);
				} else {
					tags.put(key, value);
				}
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				st.dispose();
		}
		return tags;
	}

	public Map<String, String> getRelationTagsByNodeId(long id) {
		SQLiteStatement st = null;
		Map<String, String> tags = new HashMap<String, String>();
		String sql = "SELECT relation_tags.key, relation_tags.value FROM "
				+ "way_nodes, relation_members, relation_tags WHERE way_nodes.node_id = ? "
				+ "AND relation_members.ref = way_nodes.way_id AND "
				+ "relation_tags.relation_id = relation_members.relation_id"
				+ " GROUP BY relation_tags.value ORDER BY "
				+ "relation_tags.relation_id ASC";
		String key = null, value = null;
		try {
			st = db.prepare(sql);
			st.bind(1, id);
			while (st.step()) {
				key = st.columnString(0);
				value = st.columnString(1);
				if (tags.containsKey(key)) {
					tags.put(key, tags.get(key) + ";" + value);
				} else {
					tags.put(key, value);
				}
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				st.dispose();
		}
		return tags;
	}

	public ArrayList<Long> getNearByIds(double latitude, double longitude) {
		SQLiteStatement st = null;
		String sql = "SELECT nodes.id FROM way_nodes, nodes WHERE "
				+ "way_nodes.node_id = nodes.id AND ABS(nodes.lat-?)<0.01 AND"
				+ " ABS(nodes.lon-?)<0.01";
		ArrayList<Long> result = new ArrayList<Long>();
		try {
			st = db.prepare(sql);
			st.bind(1, latitude);
			st.bind(2, longitude);
			while (st.step()) {
				result.add(st.columnLong(0));
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		}
		return result;
	}

	public ArrayList<Node> getNeighboursByNodeId(long id) {
		SQLiteStatement st = null;
		String sql = "SELECT way_nodes.way_id, way_nodes.local_order, way_nodes.node_id, nodes.lat,"
				+ " nodes.lon FROM way_nodes, nodes WHERE way_nodes.way_id = ? AND (way_nodes.local_order = ?+1 OR"
				+ " way_nodes.local_order = ?-1) AND way_nodes.node_id = nodes.id";
		ArrayList<long[]> ways = this.getWaysAndOrdersByNodeId(id);
		ArrayList<Node> neighbours = new ArrayList<Node>();
		Map<String, String> tags, rel_tags, node_tags, stop_tags;
		Node temp = null;
		try {
			for (long[] way : ways) {
				st = db.prepare(sql);
				st.bind(1, way[0]);
				st.bind(2, way[1]);
				st.bind(3, way[1]);
				while (st.step()) {
					long nId = st.columnLong(2);
					temp = new Node(nId);
					temp.setPosition(st.columnDouble(4), st.columnDouble(3));
					tags = temp.getTags();
					rel_tags = getNodeById(nId).getTags();
					if(rel_tags!=null && !rel_tags.isEmpty()){
						tags.putAll(rel_tags);
					}
					temp.setTags(tags);
					neighbours.add(temp);
				}

			}
			if (DEBUG) {
				System.out.println("node_id: " + id + " Neighbrs: "
						+ neighbours.toString());
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				st.dispose();
		}
		return neighbours;
	}

	public void close() {
		if (db != null && db.isOpen()) {
			db.dispose();
		}
	}
}
