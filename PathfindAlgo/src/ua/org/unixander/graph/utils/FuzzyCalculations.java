package ua.org.unixander.graph.utils;

import defuzzifier.Defuzzification;
import ekm.TypeReducer;
import fuzzifier.Fuzzification;
import fuzzysum.FuzzySum;
import im1.UnitIM1;
import im2.UnitIM2;
import im3.UnitIM3;

import java.util.ArrayList;

import lio.IOLV;
import rules.DBRules;

public class FuzzyCalculations {

	private ArrayList<double[][]>[] LI, LO; // Variable for library functions
	private double[][] R; // Rules database for library functions
	private FuzzySum fsumm = new FuzzySum();
	private UnitIM1 im1 = new UnitIM1(); // IM1
	private UnitIM2 im2 = new UnitIM2(); // IM2
	private UnitIM3 im3 = new UnitIM3(); // IM3
	private Fuzzification fuzz = new Fuzzification(); // Fuzzification
	private DBRules rs = new DBRules(); // Rules database I\O
	private IOLV lio = new IOLV("LI"); // Variables I\O
	private static String PATH = "", MODEL_PATH = "\\model\\", LI_FILE = "LI.xml",
			LO_FILE = "LO.xml", R_FILE = "Rules.xml";

	public FuzzyCalculations(String path) {
		PATH = path;
		LI = this.loadVars(PATH + MODEL_PATH + LI_FILE, "LI");
		LO = this.loadVars(PATH + MODEL_PATH + LO_FILE, "LO");
		R = this.loadRules(PATH + MODEL_PATH + R_FILE);
	}
	
	public FuzzyCalculations() {

	}

	/**
	 * Load linguistic variables from file
	 * 
	 * @param filePath
	 *            - path to file with variables
	 * @param varType
	 *            - type of variable (LO or LI)
	 * @return variables structure for library
	 */
	public ArrayList<double[][]>[] loadVars(String filePath, String varType) {
		ArrayList<double[][]>[] variable = null;
		lio.changeMode(varType);
		try {
			variable = lio.ReadFromXml(filePath);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return variable;
	}

	/**
	 * Load Rules from file path
	 * 
	 * @param path
	 *            - path to file with rules
	 * @return rules structure for library
	 */
	public double[][] loadRules(String path) {
		double[][] rules = null;
		try {
			rules = rs.ReadFromXml(path);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return rules;
	}

	/**
	 * Do all the calculations with the library
	 * 
	 * @return result of calculations
	 */
	public double toCalculate(double... IN) {
		if (LI == null || LO == null || R == null) {
			return -1;
		}
		double res = 0;
		TypeReducer tr = new TypeReducer();
		Defuzzification defuzz = new Defuzzification();
		try {
			double[][] M = fuzz.GetMatrixM(IN, LI);
			im1.CalcRateRules(M, R, IN.length);
			double[][][] Tact = im1.outTact();
			double[][] Ract = im1.outRact();
			ArrayList<double[][]> F = im2.CalcActMF(Ract, Tact, LO);
			double[][] Y = im3.CalcResultMF(F);
			double[] TR = tr.GetTypeReduceFS(Y);
			res = defuzz.GetValue(TR);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return res;
	}
	
	public double toCalculate(double[]... IN) {
		if (LI == null || LO == null || R == null) {
			return 0;
		}
		int size = IN.length;
		double[][] Ys;
		double[][][] temp=new double[size][][];
		double res = 0;
		TypeReducer tr = new TypeReducer();
		Defuzzification defuzz = new Defuzzification();
		try {
			for (int i = 0; i < size; i++) {
				double[][] M = fuzz.GetMatrixM(IN[i], LI);
				im1.CalcRateRules(M, R, IN[i].length);
				double[][][] Tact = im1.outTact();
				double[][] Ract = im1.outRact();
				ArrayList<double[][]> F = im2.CalcActMF(Ract, Tact, LO);
				double[][] Y = im3.CalcResultMF(F);
				temp[i]=Y;
			}
			Ys = fsumm.getFuzzySum(temp);
			double[] TR = tr.GetTypeReduceFS(Ys);
			res = defuzz.GetValue(TR);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return res;
	};
	
	public double[][] calculateFuzzyValue(double... IN){
		if (LI == null || LO == null || R == null) {
			return null;
		}
		double[][] Y = null;
		try {
			double[][] M = fuzz.GetMatrixM(IN, LI);
			im1.CalcRateRules(M, R, IN.length);
			double[][][] Tact = im1.outTact();
			double[][] Ract = im1.outRact();
			ArrayList<double[][]> F = im2.CalcActMF(Ract, Tact, LO);
			Y = im3.CalcResultMF(F);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return Y;
	}
	
	public double[][] fuzzySumm(double[][]... Ys) {
		FuzzySum fsumm = new FuzzySum();
		double[][] Ysumm = null;
		Ysumm = fsumm.getFuzzySum(Ys);
		return Ysumm;
	}
	
	public double calculateResult(double[][] Ys) {
		double result = -1;
		TypeReducer tr = new TypeReducer();
		Defuzzification defuzz = new Defuzzification();
		try {
			double[] TR = tr.GetTypeReduceFS(Ys);
			result = defuzz.GetValue(TR);
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
		}
		return result;
	}
	
	public double calculateResult(double[][]... Ys){
		double result = -1;
		double[][] Ysumm = this.fuzzySumm(Ys);
		result = this.calculateResult(Ysumm);
		return result;
	}
}
