package ua.org.unixander.graph.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import ua.org.unixander.graph.Edge;
import ua.org.unixander.graph.Graph;
import ua.org.unixander.graph.Node;

public class GraphBuilder {
	private Graph graph;
	private SQLiteConnector db;
	private LinkedList<Node> nodes;
	private LinkedList<Node> processed;
	public static long startId = 834662134; //1280287429;//1067363212;1326181881;// 
	private FuzzyCalculations fuzzy;
	
	public GraphBuilder() {
		this.db = new SQLiteConnector();
		nodes = new LinkedList<Node>();
		processed = new LinkedList<Node>();
		graph = new Graph();
		String path = "";
		try {
			path = new File(".").getCanonicalPath();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} 
		fuzzy = new FuzzyCalculations(path);
	}

	public void loadGraphFromDB() {
		Node temp = null;
		ArrayList<Node> nbrs = null;
		try {
			db.open();
			temp = db.getNodeById(startId);
			graph.addNode(temp);
			nodes.add(temp);
			while(!nodes.isEmpty()){
				temp = nodes.remove();
				processed.add(temp);
				nbrs = db.getNeighboursByNodeId(temp.getId());
				if(temp.getId() == 886553421l){
					System.out.println();
				}
				if (nbrs != null) {
					for (Node n : nbrs) {
						graph.addNode(n);
						Edge edge = new Edge(temp, n);
						Map<String, String> tags = getWayTagsForEdge(temp, n);
						if(tags!=null && !tags.isEmpty()){
							edge.setTags(tags);
						}
						graph.addEdge(edge);
						if (!graph.isOneDirection()) {
							edge = new Edge(n, temp);
							if(tags!=null && !tags.isEmpty()){
								edge.setTags(tags);
							}
							graph.addEdge(edge);
						}
						if(processed.indexOf(n) == -1 && nodes.indexOf(n) == -1){ 
							nodes.add(n);
						}
					}
					System.out.println("Nodes loaded: "+graph.getNodes().size()+"\r");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			db.close();
		}
	}

	public Map<String, String> getWayTagsForEdge(Node src, Node dest){
		HashMap<String, String> result = new HashMap<String, String>();
		ArrayList<Long> ways1, ways2;
		ways1 =  db.getWaysByNodeId(src.getId());
		ways2 = db.getWaysByNodeId(dest.getId());
		ArrayList<Long> common = new ArrayList<Long>();
		for(Long way1: ways1){
			if(ways2.indexOf(way1)>-1){
				common.add(way1);
				result.putAll(db.getTagsByWayId(way1));
			}
		}
		return result;
	}

	public Graph getGraph() {
		return this.graph;
	}

}
