package ua.org.unixander.graph;

import java.util.ArrayList;
import java.util.HashMap;

public class ExtendedEdge extends Edge {
	private ArrayList<Long> nodes = new ArrayList<Long>();

	
	public ExtendedEdge(Node... input ){
		super(input[0],input[1]);
		for(int i=0;i<input.length;i++){
			this.addNode(input[i]);
		}
	}
	
	public ExtendedEdge(Node source, Node destination) {
		super(source, destination);
		this.addNode(source);
		this.addNode(destination);
	}

	public ExtendedEdge(Node source, Node destination, double weight) {
		super(source, destination, weight);
		this.addNode(source);
		this.addNode(destination);
	}

	public ExtendedEdge(Node source, Node destination,
			HashMap<String, String> tags, double weight) {
		super(source, destination, tags, weight);
		this.addNode(source);
		this.addNode(destination);
	}

	public void addNode(Node node) {
		nodes.add(node.getId());
		this.weight += calcManhattanDistance(destination, node);
		this.destination = node;
	}

	public ArrayList<Long> getNodes() {
		return this.nodes;
	}

	private double calcManhattanDistance(Node source, Node destination) {
		if (source.getPosition() != null && destination.getPosition() != null) {
			return Math.abs(source.getPosition().getLatitude()
					- destination.getPosition().getLatitude())
					+ Math.abs(source.getPosition().getLongitude()
							- destination.getPosition().getLongitude());
		} else
			return 0;
	}

	@SuppressWarnings("unused")
	private double calcDistance(Node source, Node destination) {
		double result = 0, x1, x2, y1, y2;
		if (source.getPosition() != null && destination.getPosition() != null) {
			x1 = source.getPosition().getLatitude();
			x2 = destination.getPosition().getLatitude();
			y1 = source.getPosition().getLongitude();
			y2 = destination.getPosition().getLongitude();
			result = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
		}
		return result;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(Long i:nodes){
			sb.append(i+" -> ");
		}
		sb.delete(sb.lastIndexOf(" -> "),sb.length());
		return sb.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExtendedEdge other = (ExtendedEdge) obj;
		if (this.source.equals(other.source)
				&& this.destination.equals(other.destination)
				&& this.nodes.equals(other.nodes)) {
			return true;
		}
		return false;
	}

}
