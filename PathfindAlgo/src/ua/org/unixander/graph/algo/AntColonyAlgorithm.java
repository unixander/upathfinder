package ua.org.unixander.graph.algo;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AntColonyAlgorithm {

	public static final double ALPHA = 0.1d;
	public static final double BETA = 2.0d;

	public static final double Q = 1.0d;
	public static double PHEROMONE_PERSISTENCE = 0.5d;
	public static final double INITIAL_PHEROMONES = 0.8d;

	public final int NUM_ANTS = 256;
	private final int POOL_SIZE = Runtime.getRuntime()
			.availableProcessors();

	private final ExecutorService THREAD_POOL = Executors
			.newFixedThreadPool(POOL_SIZE);

	private final ExecutorCompletionService<WalkedWay> antCompletionService = new ExecutorCompletionService<WalkedWay>(
			THREAD_POOL);

	double[][] matrix;
	double[][] invertedMatrix;

	private double[][] pheromones;
	private Object[][] mutexes;
	private int start,finish;
	private int[] bestWay;

	public AntColonyAlgorithm(double[][] matrix, int start, int finish) throws IOException {
		this.start = start;
		this.finish = finish;
		this.matrix = matrix;
		invertedMatrix = invertMatrix();
		pheromones = initializePheromones();
		mutexes = initializeMutexObjects();
		PHEROMONE_PERSISTENCE = calculatePheromonePersistance();
	}

	private final Object[][] initializeMutexObjects() {
		final Object[][] localMatrix = new Object[matrix.length][matrix.length];
		int rows = matrix.length;
		for (int columns = 0; columns < matrix.length; columns++) {
			for (int i = 0; i < rows; i++) {
				localMatrix[columns][i] = new Object();
			}
		}

		return localMatrix;
	}

	final double readPheromone(int x, int y) {
		return pheromones[x][y];
	}

	final void adjustPheromone(int x, int y, double newPheromone) {
		synchronized (mutexes[x][y]) {
			final double result = calculatePheromones(pheromones[x][y],
					newPheromone);
			if (result >= 0.0d) {
				pheromones[x][y] = result;
			} else {
				pheromones[x][y] = 0;
			}
		}
	}
	
	private final double calculatePheromonePersistance(){
		int size = matrix.length;
		double sum = 0.0d;
		for(int i = 0; i < size;i++){
			for(int j = 0; j<size;j++){
				sum += matrix[i][j];
			}
		}
		return ((double)size*size)/sum;
	}

	private final double calculatePheromones(double current, double newPheromone) {
		final double result = (1 - AntColonyAlgorithm.PHEROMONE_PERSISTENCE)
				* current + newPheromone;
		return result;
	}

	private final double[][] initializePheromones() {
		final double[][] localMatrix = new double[matrix.length][matrix.length];
		int rows = matrix.length;
		for (int columns = 0; columns < matrix.length; columns++) {
			for (int i = 0; i < rows; i++) {
				localMatrix[columns][i] = INITIAL_PHEROMONES;
			}
		}

		return localMatrix;
	}

	private final double[][] invertMatrix() {
		double[][] local = new double[matrix.length][matrix.length];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				local[i][j] = invertDouble(matrix[i][j]);
			}
		}
		return local;
	}

	private final double invertDouble(double distance) {
		if (distance == 0d)
			return 0d;
		else
			return 1.0d / distance;
	}

	public double calculate() throws InterruptedException, ExecutionException {

		WalkedWay bestDistance = null;

		int antsSend = 0;
		int antsDone = 0;
		int antsWorking = 0;
		for (int antNumber = 0; antNumber < NUM_ANTS; antNumber++) {
			antCompletionService.submit(new Ant(this,start,finish));
			antsSend++;
			antsWorking++;
			while (antsWorking >= POOL_SIZE) {
				WalkedWay way = antCompletionService.take().get();
				//double dist = Sequential2OptRunner.getRouteCost(way.way, matrix, 0, way.way.length-1);
				//System.out.println(Arrays.toString(way.way)+" "+way.distance+" "+dist);
				if (bestDistance == null
						|| way.distance < bestDistance.distance) {
					bestDistance = way;
				}
				antsDone++;
				antsWorking--;
			}
		}
		final int left = antsSend - antsDone;

		for (int i = 0; i < left; i++) {
			WalkedWay way = antCompletionService.take().get();
			if (bestDistance == null || way.distance < bestDistance.distance) {
				bestDistance = way;
			}
		}

		THREAD_POOL.shutdownNow();
		//Sequential2OptRunner opt2 = new Sequential2OptRunner(bestDistance, matrix);
	    //while(opt2.runOnce()){};
	    //WalkedWay result = opt2.getRoute();
	    
		this.bestWay = bestDistance.way.clone();
		return bestDistance.distance;

	}

	public int[] getBestWay(){
		return this.bestWay;
	}

	static class WalkedWay {
		int[] way;
		double distance;

		public WalkedWay(int[] way, double distance) {
			super();
			this.way = way;
			this.distance = distance;
		}
	}

}
