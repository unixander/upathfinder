package ua.org.unixander.graph.algo;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

import ua.org.unixander.graph.Graph;
import ua.org.unixander.graph.Node;
import ua.org.unixander.graph.extendedalgo.applier.NodeApply;

public class AStarAlgorithm {
	private static double distance;
	private static List<Node> list;
	private static NodeApply applier;
	
	public static void setApplier(NodeApply appl) {
		applier = appl;
	}
	
	public static List<Node> search(Graph graph, Node source, Node target) {
		distance = 0;
		Map<Long, AStarNode> openSet = new HashMap<Long, AStarNode>();
		PriorityQueue<AStarNode> pQueue = new PriorityQueue<AStarNode>(20,
				new AStarNodeComparator());
		Map<Long, AStarNode> closeSet = new HashMap<Long, AStarNode>();
		AStarNode start = new AStarNode(source, 0, graph.calcManhattanDistance(
				source, target));
		openSet.put(source.getId(), start);
		pQueue.add(start);
		AStarNode goal = null;

		while (openSet.size() > 0) {
			AStarNode x = pQueue.poll();
			openSet.remove(x.getId());
			if (x.getId() == target.getId()) {
				goal = x;
				break;
			} else {
				closeSet.put(x.getId(), x);
				Set<Node> neighbors = graph.getAdjacency(x.getNode());
				for (Node neighbor : neighbors) {
					AStarNode visited = closeSet.get(neighbor.getId());
					if (visited == null && applier.canApplyEdge(x.getNode(), neighbor)) {
						double g = x.getG()
								+ graph.calcDistance(x.getNode(),
										neighbor);
						AStarNode nbr = openSet.get(neighbor.getId());

						if (nbr == null) {
							nbr = new AStarNode(neighbor, g,
									graph.calcDistance(neighbor,
											target));
							nbr.setCameFrom(x);
							openSet.put(neighbor.getId(), nbr);
							pQueue.add(nbr);
						} else if (g < nbr.getG()) {
							nbr.setCameFrom(x);
							nbr.setG(g);
							nbr.setH(graph.calcDistance(neighbor,
									target));
						}
					}
				}
			}
		}

		if (goal != null) {
			ArrayList<Node> stack = new ArrayList<Node>();
			list = new ArrayList<Node>();
			stack.add(goal.getNode());
			AStarNode parent = goal.getCameFrom();
			while (parent != null) {
				if (parent.getCameFrom() != null)
					distance += graph.calcDistance(parent.getNode(), parent
							.getCameFrom().getNode());
				stack.add(parent.getNode());
				parent = parent.getCameFrom();
			}
			while (stack.size() > 0) {
				list.add(stack.remove(stack.size() - 1));
			}
			return list;
		}
		return null;
	}

	public static long[] getWayIds() {
		long[] result = new long[list.size()];
		for (int i = 0; i < list.size(); i++) {
			result[i] = list.get(i).getId();
		}
		return result;
	}

	public static double getDistance() {
		return distance;
	}

	public static class AStarNode {
		private Node node;
		private AStarNode cameFrom;
		private double g, h;

		public AStarNode(Node node, double g, double h) {
			this.g = g;
			this.h = h;
			this.node = node;
		}

		public long getId() {
			return node.getId();
		}

		public Node getNode() {
			return node;
		}

		public AStarNode getCameFrom() {
			return cameFrom;
		}

		public double getG() {
			return g;
		}

		public double getH() {
			return h;
		}

		public void setNode(Node node) {
			this.node = node;
		}

		public void setCameFrom(AStarNode cameFrom) {
			this.cameFrom = cameFrom;
		}

		public void setG(double g) {
			this.g = g;
		}

		public void setH(double h) {
			this.h = h;
		}

		public double getF() {
			return g + h;
		}
	}

	private static class AStarNodeComparator implements Comparator<AStarNode> {

		public int compare(AStarNode first, AStarNode second) {
			if (first.getF() < second.getF()) {
				return -1;
			} else if (first.getF() > second.getF()) {
				return 1;
			} else {
				return 0;
			}
		}
	}
}
