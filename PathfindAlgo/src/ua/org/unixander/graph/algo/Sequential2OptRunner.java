package ua.org.unixander.graph.algo;

import java.util.Arrays;

import ua.org.unixander.graph.algo.AntColonyAlgorithm.WalkedWay;

public class Sequential2OptRunner {
	private double[][] distances;
	private WalkedWay route;
	
	public Sequential2OptRunner(int[] startRoute, double[][] distances, double distance){
		this.route = new WalkedWay(startRoute, distance);
		this.distances = distances;
	}
	
	public Sequential2OptRunner(WalkedWay startRoute, double[][] distances){
		this.route = startRoute;
		this.distances = distances;
	}
	
	public boolean runOnce(){
		boolean changed = false;
		for(int i = 2; i < route.way.length-1;i++){
			WalkedWay result = getBestRouteSplit(this.route,i);
			if(result.distance<this.route.distance){
				changed = true;
				this.route = result;
			}
		}
		return changed;
	}
	
	public WalkedWay getRoute(){
		return this.route;
	}
	
	public static double getRouteCost(int[] route, double[][] distances,
			int from, int to) {
		double result = 0;
		for (int i = from; i < to; i++) {
			result += distances[route[i]][route[i + 1]];
		}
		return result;
	}
	
	public int[] reverseArray(int[] array){
		int[] result = new int[array.length];
		for(int i = 0; i< array.length;i++){
			result[i] = array[array.length-1-i];
		}
		return result;
	}
	
	public int[] getSubArray(int[] array, int from, int to){
		return Arrays.copyOfRange(array, from, to+1);
	}
	
	private WalkedWay getBestRouteSplit(WalkedWay route, int splitIndex){
		WalkedWay next;
		double ab = getRouteCost(route.way, distances, 0, splitIndex-1);
		int[] reverse = this.reverseArray(route.way), way = route.way;
		int reverseSplitIndex = route.way.length - splitIndex;
		
		double dc = getRouteCost(reverse, distances, 0, reverseSplitIndex-1);
		next = new WalkedWay(route.way, route.distance);
		
		double temp = ab + distances[way[splitIndex-1]][way[way.length-1]]+dc;
		if(temp < next.distance){
			int[] array1 = getSubArray(way, 0, splitIndex-1),
					array2 = getSubArray(reverse, 0, reverseSplitIndex-1);
			int[] tempWay = new int[array1.length + array2.length];
			System.arraycopy(array1, 0, tempWay, 0, array1.length);
			System.arraycopy(array2, 0, tempWay, array1.length, array2.length);
			next = new WalkedWay(tempWay, temp);
		}
		return next;
	}
	
}
