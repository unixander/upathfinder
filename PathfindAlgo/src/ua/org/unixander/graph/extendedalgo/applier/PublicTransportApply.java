package ua.org.unixander.graph.extendedalgo.applier;

import java.util.Map;

import ua.org.unixander.graph.Edge;
import ua.org.unixander.graph.Graph;
import ua.org.unixander.graph.Node;

public class PublicTransportApply implements NodeApply {

	private Graph graph;

	public PublicTransportApply(Graph graph) {
		this.graph = graph;
	}

	@Override
	public boolean canApply(Node node) {
		return false;
	}

	@Override
	public boolean canApplyEdge(Node src, Node dest) {
		if (graph == null){
			System.out.println("Graph load error");
			return false;
		}
		Map<String, String> tags1 = src.getTags(), tags2 = dest.getTags();
		Edge edge = graph.findEdge(src, dest);
		if (edge == null) {
			edge = graph.findEdge(dest, src);
		}
		if (edge == null)
			return false;
		if(tags1!=null && tags2!=null){
			if(tags1.containsKey("ref") && tags2.containsKey("ref")){
				return true;
			}
		}
		return false;
	}
}
