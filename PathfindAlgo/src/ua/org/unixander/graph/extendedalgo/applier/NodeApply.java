package ua.org.unixander.graph.extendedalgo.applier;

import ua.org.unixander.graph.Node;

public interface NodeApply {
	public boolean canApply(Node node);
	public boolean canApplyEdge(Node src, Node dest);
}
