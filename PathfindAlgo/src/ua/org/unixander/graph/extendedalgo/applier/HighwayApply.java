package ua.org.unixander.graph.extendedalgo.applier;

import java.util.Map;

import ua.org.unixander.graph.Edge;
import ua.org.unixander.graph.Graph;
import ua.org.unixander.graph.Node;

public class HighwayApply implements NodeApply {

	private Graph graph;

	public HighwayApply(Graph graph) {
		this.graph = graph;
	}

	@Override
	public boolean canApply(Node node) {
		return false;
	}

	@Override
	public boolean canApplyEdge(Node src, Node dest) {
		if (graph == null){
			System.out.println("Graph load error");
			return false;
		}
		Edge edge = graph.findEdge(src, dest);
		if (edge == null) {
			edge = graph.findEdge(dest, src);
		}
		if (edge == null)
			return false;
		Map<String, String> tags = edge.getTags();
		if (tags != null) {
			if (tags.containsKey("highway")
					&& !tags.get("highway").equalsIgnoreCase("footway")) {
				return true;
			}
		}
		return false;
	}
}
