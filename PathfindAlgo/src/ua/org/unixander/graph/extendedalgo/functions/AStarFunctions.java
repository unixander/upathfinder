package ua.org.unixander.graph.extendedalgo.functions;

public interface AStarFunctions<T,U> {
	/**
	 * Knowledge-plus-heuristic function
	 * @param g - result of knowledge function
	 * @param h - result of heuristic function
	 * @return
	 */
	public double F(double g, double h);
	
	/**
	 * Knowledge function
	 * @param args
	 * @return
	 */
	public double G(T... args);
	
	/**
	 * Heuristic function
	 * @param args
	 * @return
	 */
	public double H(U... args);
	
}
