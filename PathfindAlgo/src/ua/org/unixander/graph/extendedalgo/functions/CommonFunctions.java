package ua.org.unixander.graph.extendedalgo.functions;

public class CommonFunctions implements AStarFunctions<Double, Double> {

	@Override
	public double F(double g, double h) {
		return g + h;
	}

	@Override
	public double G(Double... args) {
		double result = 0f;
		for (int i = 0; i < args.length; i++) {
			result+=args[i];
		}
		return result;
	}

	@Override
	public double H(Double... args) {
		double result = 0f;
		for (int i = 0; i < args.length; i++) {
			result+=args[i];
		}
		return result;
	}

}
