package ua.org.unixander.graph.extendedalgo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Queue;

public class BranchAndBoundExt {

	@SuppressWarnings("unused")
	private double[][] weights;
	private int n;
	private ArrayList<Route> routes = new ArrayList<Route>();
	private double bestRoute;
	private boolean isLoop = false;

	@SuppressWarnings("rawtypes")
	public class Route implements Comparable {
		int[] sequence;
		int index;
		double dist;
		int nRoutes = 0;

		private Route(int[] vect, int index, double[][] wt) {
			dist = 0;
			for (int k = 1; k <= index; k++) {
				dist += wt[vect[k - 1]][vect[k]];
			}
			if (index == (isLoop ? n - 1 : n)) {
				dist += wt[vect[n - 1]][vect[n - 2]];
			}
			sequence = new int[n];
			System.arraycopy(vect, 0, sequence, 0, n);
			this.index = index;
			nRoutes++;
		}

		public int compareTo(Object o) {
			Route route = (Route) o;
			return (int) Double.compare(this.dist, route.dist);
		}

		public String toString() {
			StringBuilder val = new StringBuilder(
					Integer.toString(sequence[0] + 1));
			for (int k = 1; k < n; k++) {
				val.append(", " + Integer.toString(sequence[k] + 1));
			}
			val.append(String.format(" with distance %6.6f", dist));
			return val.toString();
		}

		public int[] getSequence() {
			return sequence;
		}

		public double getDist() {
			return dist;
		}

	}

	public BranchAndBoundExt() {
		this.bestRoute = Integer.MAX_VALUE;
	}

	public void swap(int[] x, int i, int j) {
		if (i == j)
			return;
		int temp = x[i];
		x[i] = x[j];
		x[j] = temp;
	}

	public Route calculate(double[][] weights, int[] sequence) {
		this.weights = weights;
		this.n = weights.length;
		int[] holder;
		Queue<Route> queue = new PriorityQueue<Route>();

		queue.add(new Route(sequence, 1, weights));

		while (!queue.isEmpty()) {
			Route current = queue.poll();
			int index = current.index;
			sequence = current.sequence;
			holder = sequence.clone();

			if (index == n - 1) {
				if (weights[sequence[n - 1]][sequence[0]] > 0) {
					if (current.dist < bestRoute) {
						bestRoute = current.dist;
						routes.add(current);
					}
				}
				;
			} else {
				for (int i = index; i < n - 1; i++) {
					swap(sequence, index, i);
					if (weights[sequence[index - 1]][sequence[index]] < 0)
						continue;
					Route route = new Route(sequence, index + 1, weights);
					if (route.dist < this.bestRoute)
						queue.add(route);
				}
				sequence = holder;
			}
		}
		return getOptimal();
	}

	@SuppressWarnings("unchecked")
	public Route getOptimal() {
		if (routes.size() == 0)
			return null;
		Collections.sort(routes);
		return routes.get(0);
	}

	public double getDistance(){
		return this.bestRoute;
	}
	
	public int[] getBestWay(){
		return this.getOptimal().sequence;
	}
	
	public ArrayList<Route> getRoutes() {
		return this.routes;
	}
}
