package ua.org.unixander.graph.extendedalgo;

import java.util.Random;
import java.util.concurrent.Callable;

import ua.org.unixander.graph.extendedalgo.AntColonyAlgorithmExt.WalkedWay;

public final class Ant implements Callable<WalkedWay> {

	  private final AntColonyAlgorithmExt instance;
	  private double distanceWalked = 0.0d;
	  private final int start, finish;
	  private final boolean[] visited;
	  private final int[] way;
	  private int toVisit;
	  private final Random random = new Random(System.nanoTime());

	  public Ant(AntColonyAlgorithmExt instance, int start, int finish) {
	    super();
	    this.instance = instance;
	    this.visited = new boolean[instance.matrix.length];
	    visited[start] = true;
	    visited[finish] = true;
	    toVisit = visited.length - 2;
	    this.start = start;
	    this.finish = finish;
	    this.way = new int[visited.length];
	  }

	  private final int getNextProbableNode(int y) {
	    if (toVisit > 0) {
	      int unvisited = -1;
	      final double[] weights = new double[visited.length];

	      double columnSum = 0.0d;
	      for (int i = 0; i < visited.length; i++) {
	        columnSum += Math.pow(instance.readPheromone(y, i),
	            AntColonyAlgorithmExt.ALPHA)
	            * Math.pow(instance.invertedMatrix[y][i],
	                AntColonyAlgorithmExt.BETA);
	      }

	      double sum = 0.0d;
	      for (int x = 0; x < visited.length; x++) {
	        if (!visited[x]) {
	          weights[x] = calculateProbability(x, y, columnSum);
	          sum += weights[x];
	          unvisited = x;
	        }
	      }

	      if (sum == 0.0d)
	        return unvisited;

	      // weighted indexing stuff
	      double pSum = 0.0d;
	      for (int i = 0; i < visited.length; i++) {
	        pSum += weights[i] / sum;
	        weights[i] = pSum;
	      }

	      final double r = random.nextDouble();
	      for (int i = 0; i < visited.length; i++) {
	        if (!visited[i]) {
	          if (r <= weights[i]) {
	            return i;
	          }
	        }
	      }

	    }
	    return -1;
	  }

	  /*
	   * (pheromones ^ ALPHA) * ((1/length) ^ BETA) divided by the sum of all rows.
	   */
	  private final double calculateProbability(int row, int column, double sum) {
	    final double p = Math.pow(instance.readPheromone(column, row),
	        AntColonyAlgorithmExt.ALPHA)
	        * Math.pow(instance.invertedMatrix[column][row],
	            AntColonyAlgorithmExt.BETA);
	    return p / sum;
	  }

	  @Override
	  public final WalkedWay call() throws Exception {

	    int lastNode = start;
	    int next = start;
	    int i = 0;
	    while ((next = getNextProbableNode(lastNode)) != -1) {
	      way[i] = lastNode;
	      i++;
	      distanceWalked += instance.matrix[lastNode][next];
	      final double phero = (AntColonyAlgorithmExt.Q / (distanceWalked));
	      instance.adjustPheromone(lastNode, next, phero);
	      visited[next] = true;
	      lastNode = next;
	      toVisit--;
	    }
	    distanceWalked += instance.matrix[lastNode][finish];
	    way[i] = lastNode;
	    way[i+1] = finish;
	    
	    WalkedWay result = new WalkedWay(way, distanceWalked);
	    //Sequential2OptRunner opt2 = new Sequential2OptRunner(result, instance.matrix);
	    //while(opt2.runOnce()){};
	    //result = opt2.getRoute();
	    
	    return result;
	  }
	}

