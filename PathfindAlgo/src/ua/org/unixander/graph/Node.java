package ua.org.unixander.graph;

import java.util.HashMap;
import java.util.Map;

public class Node {
	private long id;
	private Position position;
	private Map<String, String> tags;
	private static boolean DEBUG = true;

	public Node(long id) {
		this.id = id;
		this.tags = new HashMap<String, String>();
	}
	
	public Node(double latitude, double longitude) {
		this.position = new Position(longitude, latitude);
	}

	public void setTags(Map<String, String> tags) {
		this.tags.putAll(tags);
	}

	public Map<String, String> getTags() {
		return this.tags;
	}

	public void addTag(String key, String value) {
		this.tags.put(key, value);
	}

	public String removeTag(String key) {
		return this.tags.remove(key);
	}

	public String getTag(String key) {
		return this.tags.get(key);
	}

	public boolean checkTagKey(String key) {
		return this.tags.containsKey(key);
	}

	public boolean checkTagValue(String value) {
		return this.tags.containsValue(value);
	}

	public void setPosition(double longitude, double latitude) {
		this.setPosition(new Position(longitude, latitude));
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Position getPosition() {
		return this.position;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		return Long.toString(this.id).hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (this.id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		if (DEBUG)
			return Long.toString(this.id);
			//return "path.addPoint(new GeoPoint("+position.getLatitude()+","+position.getLongitude()+");";
			//return "Node(" + this.id + "[" + position.toString() + "]" + "{"
			//		+ tags.toString() + "}" + ")";
		else
			return "Node(" + this.id + ")";
	}

}
