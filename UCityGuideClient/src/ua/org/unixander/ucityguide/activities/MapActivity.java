package ua.org.unixander.ucityguide.activities;

import java.util.ArrayList;

import org.osmdroid.bonuspack.overlays.ExtendedOverlayItem;
import org.osmdroid.bonuspack.overlays.ItemizedOverlayWithBubble;
import org.osmdroid.bonuspack.routing.MapQuestRoadManager;
import org.osmdroid.bonuspack.routing.Road;
import org.osmdroid.bonuspack.routing.RoadManager;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.PathOverlay;

import ua.org.unixander.ucityguide.R;
import ua.org.unixander.ucityguide.UCityGuideApp;
import ua.org.unixander.ucityguide.model.SearchType;
import ua.org.unixander.ucityguide.model.ServerCommands;
import ua.org.unixander.ucityguide.model.UPoint;
import ua.org.unixander.ucityguide.model.USystem;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

@SuppressWarnings("unused")
public class MapActivity extends Activity {

	private MapView mapView;
	private Dialog dialog;
	private Context context;
	private ProgressBar progressBar;
	private TextView infoText;
	private USystem system;
	private Button btnShow;
	private ArrayList<UPoint> points;
	private SearchType type;
	private LocationManager mLocationManager;
	private Location mLocation;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.system = USystem.getInstance();
		setContentView(R.layout.map_activity);
		this.context = this;
		this.type = system.getType();
		
		mLocationManager = (LocationManager) this
				.getSystemService(Context.LOCATION_SERVICE);
		mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, new ULocationListener());
		
		btnShow = (Button) findViewById(R.id.btnShow);
		btnShow.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				showPointsList();
			}
		});
		mapView = (MapView) findViewById(R.id.map);
		mapView.setTileSource(TileSourceFactory.MAPQUESTOSM);
		mapView.setLongClickable(true);
		mapView.setBuiltInZoomControls(true);
		mapView.setMultiTouchControls(true);
		mapView.getController().setZoom(16);

		boolean isConnected = checkInternetConnection();
		if (isConnected) {
			dialog = new Dialog(this);
			dialog.setTitle("Searching progress");
			dialog.setContentView(R.layout.dialog_search_route);
			progressBar = (ProgressBar) dialog.findViewById(R.id.progressBar);
			infoText = (TextView) dialog.findViewById(R.id.info);
			dialog.show();
			new Calculations().execute();
		}

	}

	private boolean checkInternetConnection() {
		ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = manager.getActiveNetworkInfo();
		if (info == null || !info.isAvailable() || !info.isConnected()) {
			Toast.makeText(getApplicationContext(),
					"Error connecting to Internet. Please check",
					Toast.LENGTH_LONG).show();
			return false;
		} else
			return true;
	}

	private ArrayList<String> routesIntersect(String first, String second) {
		String[] route1, route2;
		ArrayList<String> result = new ArrayList<String>();
		route1 = first.split(";");
		route2 = second.split(";");
		for (int i = 0; i < route1.length; i++) {
			for (int j = 0; j < route2.length; j++) {
				if (route1[i].equals(route2[j])) {
					result.add(route1[i]);
				}
			}
		}
		return result;
	}

	private ArrayList<String> intersectList(ArrayList<String> first,
			ArrayList<String> second) {
		ArrayList<String> result = new ArrayList<String>();
		for (String s : first) {
			if (second.contains(s)) {
				result.add(s);
			}
		}
		return result;
	}

	private void addExtendedOverlay() {
		ArrayList<ExtendedOverlayItem> roadItems = new ArrayList<ExtendedOverlayItem>();
		ItemizedOverlayWithBubble<ExtendedOverlayItem> roadNodes = new ItemizedOverlayWithBubble<ExtendedOverlayItem>(
				this, roadItems, mapView);
		Drawable marker = getResources().getDrawable(R.drawable.marker_node), stop = getResources()
				.getDrawable(R.drawable.busstop), share = getResources()
				.getDrawable(R.drawable.share), icon = getIcon();
		int index = 1, prev_stop = -1, prev_prev_stop = -1;
		ArrayList<String> first, second;
		ArrayList<UPoint> stops = new ArrayList<UPoint>();
		for (UPoint p : points) {
			ExtendedOverlayItem item = new ExtendedOverlayItem("("
					+ Integer.toString(index) + ")" + p.getTitle(),
					p.getSubtitle(), new GeoPoint(p.getLatitude(),
							p.getLongitude()), mapView.getContext());
			item.setMarkerHotspot(OverlayItem.HotspotPlace.BOTTOM_CENTER);
			if (p.isStop() && system.getType() == SearchType.PUBLIC_TRANSIT) {
				int stopSize = stops.size();
				item.setMarker(stop);
				if (stopSize > 1) {
					first = routesIntersect(p.getRawSubtitle(),
							stops.get(stopSize - 1).getRawSubtitle());
					second = routesIntersect(p.getRawSubtitle(),
							stops.get(stopSize - 2).getRawSubtitle());
					first = intersectList(first, second);
					if (first.size() > 0) {
						item.setMarker(stop);
					} else {
						int prevPos = points.indexOf(stops.get(stopSize - 1));
						roadNodes.getItem(prevPos).setMarker(share);
					}
				}
				stops.add(p);
			} else if (p.isWayPoint()) {
				item.setMarkerHotspot(OverlayItem.HotspotPlace.CENTER);
				item.setMarker(marker);
			} else {
				if (icon != null) {
					item.setMarker(icon);
				}
				mapView.getController().setCenter(
						new GeoPoint(p.getLatitude(), p.getLongitude()));
			}
			roadNodes.addItem(item);
			index++;
		}
		mapView.getOverlays().add(roadNodes);
		mapView.invalidate();
	}

	private Drawable getIcon() {
		SearchType type = system.getType();
		Drawable icon;
		switch (type) {
		case AUTO:
			icon = getResources().getDrawable(R.drawable.car);
			break;
		case PUBLIC_TRANSIT:
		default:
			icon = null;
		}
		return icon;
	}

	private void showPointsList() {
		int size;
		if(points == null){
			Toast.makeText(context, "Route not found", Toast.LENGTH_SHORT).show();
			return;
		}
		String[] titles = new String[points.size()];
		int i = 0;
		for (UPoint p : points) {
			titles[i] = Integer.toString(i + 1) + ") " + p.getTitle();
			i++;
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Points order").setItems(titles, null);
		builder.create().show();
	}

	private class Calculations extends AsyncTask<Void, Void, ServerCommands> {

		@Override
		protected ServerCommands doInBackground(Void... values) {
			boolean flag = system.calculateOptimal(mapView.getContext());
			return system.getStatus();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPostExecute(ServerCommands status) {
			super.onPostExecute(status);
			dialog.dismiss();
			if(status == null){
				status = ServerCommands.SERVER_ERROR;
			}
			switch (status) {
			case OK_RESPONSE:
				buildMap();
				break;
			case SERVER_ERROR:
				Toast.makeText(getApplicationContext(),
						"Server error. Try again later.", Toast.LENGTH_SHORT)
						.show();
				break;
			default:
				Toast.makeText(getApplicationContext(), system.getLastError(),
						Toast.LENGTH_SHORT).show();
			}
		}

		private void buildMap() {
			points = system.getRespoints();
			if (points == null) {
				points = system.getPoints();
			}
			ArrayList<PathOverlay> paths = system.getPaths();
			if (paths == null)
					return;
			for (PathOverlay path : paths) {
				mapView.getOverlayManager().add(path);
			}
			addExtendedOverlay();
		}
	}

	private class ULocationListener implements LocationListener {

		@Override
		public void onLocationChanged(Location location) {
			mLocation = location;
			//TODO: User location
			mapView.getOverlayManager();
		}

		@Override
		public void onProviderDisabled(String provider) {
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}

	}
}
