package ua.org.unixander.ucityguide.fragments;

import ua.org.unixander.ucityguide.R;
import ua.org.unixander.ucityguide.controllers.LocalDB;
import ua.org.unixander.ucityguide.controllers.ServerConnection;
import ua.org.unixander.ucityguide.utils.GPSTracker;
import android.app.Fragment;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SettingsFragment extends Fragment {

	private LocalDB dbHelper;
	private Context context;
	private EditText hostText, portText;
	private Button btnSave, btnCheck, btnGPS;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.settings, container, false);
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		//super.onViewCreated(view, savedInstanceState);
		context = getActivity().getBaseContext();
		dbHelper = new LocalDB(context);
		dbHelper.openDataBase();
		String h = dbHelper.getHost();
		String p = dbHelper.getPort();
		dbHelper.close();
		hostText = (EditText) view.findViewById(R.id.editText1);
		portText = (EditText) view.findViewById(R.id.editText2);
		btnSave = (Button) view.findViewById(R.id.button1);
		btnCheck = (Button) view.findViewById(R.id.btnCheckConnection);
		btnGPS = (Button) view.findViewById(R.id.btnGPSCheck);
		
		hostText.setText(h);
		portText.setText(p);

		btnCheck.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				String host = hostText.getText().toString();
				if (host.isEmpty()) {
					Toast.makeText(context,
							"Enter server host", Toast.LENGTH_SHORT);
					return;
				}
				int port = Integer.parseInt(portText.getText().toString());
				if (port <= 0) {
					Toast.makeText(context,
							"Enter correct server port", Toast.LENGTH_SHORT)
							.show();
					return;
				}
				ServerConnection.init(context);
				ServerConnection db = ServerConnection.getInstance();//new ServerConnection(host, port);
				try {
					if (db.open()) {
						db.close();
						Toast.makeText(context,
								"Connection to server established",
								Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(
								context,
								"Host or port is invalid. Or server is inaccesible",
								Toast.LENGTH_LONG).show();
					}
				} catch (Exception e) {
					Toast.makeText(context,
							"Something went wrong. Try again",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		btnGPS.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				GPSTracker gps = new GPSTracker(v.getContext());
				if(gps.canGetLocation()){
					double lat, lon;
					Location loc = gps.getLocation();
					if (loc == null){
						loc = gps.getLastKnownLocation();
					}
					lat = gps.getLatitude();
					lon = gps.getLongitude();
					Toast.makeText(v.getContext(), Double.toString(lat)+":"+Double.toString(lon), Toast.LENGTH_SHORT).show();
				} else {
					gps.showSettingsAlert();
				}
				
			}
		});
		
		btnSave.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				dbHelper.openDataBaseforWrite();
				String host = hostText.getText().toString();
				dbHelper.updateHost(host);
				String port = portText.getText().toString();
				dbHelper.updatePort(port);
				dbHelper.close();
				Toast.makeText(context, "Settings saved",
						Toast.LENGTH_SHORT).show();
			}
		});
	}

}
