package ua.org.unixander.ucityguide.fragments;

import java.util.ArrayList;
import ua.org.unixander.ucityguide.R;
import ua.org.unixander.ucityguide.adapters.PointsListAdapter;
import ua.org.unixander.ucityguide.dialogs.SearchOptionsDialog;
import ua.org.unixander.ucityguide.model.UPoint;
import ua.org.unixander.ucityguide.model.USystem;
import android.annotation.TargetApi;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.Button;
import android.widget.Toast;

public class PointsListViewFragment extends ListFragment {

	private Button searchBtn;
	private USystem system;

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_points_list, container,
				false);
		return view;
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		registerForContextMenu(getListView());
		system = USystem.getInstance();
		this.loadPoints();
		searchBtn = (Button) view.findViewById(R.id.searchOptimal);
		searchBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				boolean isStartFinish = system.checkStartAndFinish();
				if (!isStartFinish) {
					Toast.makeText(getActivity().getApplicationContext(),
							"You must set start and finish points",
							Toast.LENGTH_SHORT).show();
					
					return;
				}
				showDialog();
			}
		});
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser) {
			this.loadPoints();
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getActivity().getMenuInflater();
		inflater.inflate(R.menu.points_list_menu, menu);

	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();
		int position = info.position;
		switch (item.getItemId()) {
		case R.id.set_start:
			system.setStart(position);
			break;
		case R.id.set_end:
			system.setFinish(position);
			break;
		case R.id.delete:
			system.deletePoint(position);
			break;
		default:
			return super.onContextItemSelected(item);
		}
		loadPoints();
		return true;
	}
	
	private void showDialog() {
	    FragmentTransaction ft = getFragmentManager().beginTransaction();
	    Fragment prev = getFragmentManager().findFragmentByTag("dialog");
	    if (prev != null) {
	        ft.remove(prev);
	    }
	    ft.addToBackStack(null);

	    DialogFragment newFragment = SearchOptionsDialog.newInstance();
	    newFragment.show(ft, "dialog");
	}
	
	private void showPointsListChooser(int position){
		final int initial = position;
		String[] titles = new String[system.getPoints().size()];
		int i = 0;
		for(UPoint p: system.getPoints()){
			titles[i] = Integer.toString(i+1)+") "+p.getTitle();
			if(i==position) titles[i]+="*";
			i++;
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	    builder.setTitle("Choose previous point")
	           .setItems(titles, new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int which) {
	            	   system.setAfter(initial, which);
	            	   loadPoints();
	           }
	    });
	    builder.create().show();
	}
	
	private void loadPoints() {
		ArrayList<UPoint> points = system.getPoints();
		if (points == null)
			return;
		String[] titles = new String[points.size()];
		for (int i = 0; i < points.size(); i++) {
			titles[i] = points.get(i).getTitle();
		}
		PointsListAdapter adapter = new PointsListAdapter(getActivity(),
				titles, points);
		setListAdapter(adapter);
		getListView().invalidate();
	}

}
