package ua.org.unixander.ucityguide.fragments;

import java.util.ArrayList;
import ua.org.unixander.ucityguide.R;
import ua.org.unixander.ucityguide.adapters.SearchListAdapter;
import ua.org.unixander.ucityguide.model.UPoint;
import ua.org.unixander.ucityguide.model.USystem;
import android.annotation.TargetApi;
import android.app.ListFragment;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class SearchPointFragment extends ListFragment {

	private EditText editSearch;
	private Button btnSearch;
	private ArrayList<UPoint> points;
	private USystem system;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_search_point, container,
				false);

		return view;
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		system = USystem.getInstance();
		editSearch = (EditText) view.findViewById(R.id.editSearch);
		btnSearch = (Button) view.findViewById(R.id.btnSearch);
		btnSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String query = editSearch.getText().toString();
				if (query != null && query.length() > 0) {
					points = system.searchPoints(query);
					if (points != null && points.size() > 0) {
						String[] titles = new String[points.size()];
						int i = 0;
						for (UPoint p : points) {
							titles[i] = p.getTitle();
							i++;
						}
						SearchListAdapter adapter = new SearchListAdapter(
								getActivity(), titles, points);
						setListAdapter(adapter);
					} else {
						String err = system.getLastError();
						if (err != null && err.length() > 0)
							Toast.makeText(getActivity(), err,
									Toast.LENGTH_SHORT).show();
					}
				} else {
					return;
				}
			}
		});
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		final Context context = v.getContext();
		final UPoint point = points.get(position);
		new AlertDialog.Builder(v.getContext())
				.setIcon(android.R.drawable.ic_dialog_alert)
				.setTitle("Please confirm")
				.setMessage("Do you want to add this point to your list?")
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								if(system.checkInPoints(point)){
									Toast.makeText(context, "Point already exist in your route", Toast.LENGTH_SHORT).show();
									return;
								}	
								system.addPoint(point);
								Toast.makeText(context, "Point added to your route", Toast.LENGTH_SHORT).show();
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).show();

	}

}
