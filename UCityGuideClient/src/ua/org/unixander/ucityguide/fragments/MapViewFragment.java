package ua.org.unixander.ucityguide.fragments;

import java.util.ArrayList;

import org.osmdroid.bonuspack.overlays.ExtendedOverlayItem;
import org.osmdroid.bonuspack.overlays.ItemizedOverlayWithBubble;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Overlay;

import ua.org.unixander.ucityguide.R;
import ua.org.unixander.ucityguide.activities.MainActivity;
import ua.org.unixander.ucityguide.model.UPoint;
import ua.org.unixander.ucityguide.model.USystem;
import ua.org.unixander.ucityguide.utils.GPSTracker;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Canvas;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

public class MapViewFragment extends Fragment {

	protected static final String PROVIDER_NAME = LocationManager.GPS_PROVIDER;

	private MapView mapView;
	private USystem system;
	private GeoPoint center = new GeoPoint(48.008512, 37.796488);;
	private ArrayList<ExtendedOverlayItem> items;
	private View view;
	private GPSTracker gps;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		setRetainInstance(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_map, container, false);
		return view;
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		system = USystem.getInstance();
		this.gps = new GPSTracker(this.getActivity());
		GeoPoint position = null;
		if (gps.canGetLocation()) {
			position = gps.getPosition();
		} else {
			gps.showSettingsAlert();
		}
		if (position != null)
			center = position;
		mapView = (MapView) view.findViewById(R.id.map);
		mapView.setTileSource(TileSourceFactory.MAPQUESTOSM);
		mapView.setLongClickable(true);
		mapView.setBuiltInZoomControls(true);
		mapView.setMultiTouchControls(true);
		mapView.getController().setZoom(16);
		mapView.getController().setCenter(center);
		this.loadPoints(false);
		this.reloadMapOverlays();
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser) {
			this.loadPoints(false);
			this.reloadMapOverlays();
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case R.id.action_location:
			GeoPoint position = null;
			if (gps.canGetLocation()) {
				position = gps.getPosition();
			} else {
				gps.showSettingsAlert();
			}
			if(position != null)
				mapView.getController().animateTo(new GeoPoint(position.getLatitude(), position.getLongitude()));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	    inflater.inflate(R.menu.map_menu, menu);
	}

	private void reloadMapOverlays() {
		if (mapView == null)
			return;
		if (mapView.getOverlays() != null && mapView.getOverlays().size() > 0)
			mapView.getOverlays().clear();
		if (this.items != null && this.items.size() > 0)
			mapView.getOverlays().add(
					new ItemizedOverlayWithBubble<ExtendedOverlayItem>(
							getActivity().getApplicationContext(), this.items,
							mapView));
		mapView.getOverlays().add(
				new MapOverlay(getActivity().getApplicationContext()));
		mapView.invalidate();
	}

	private void loadPoints(boolean isAfterAdd) {
		items = new ArrayList<ExtendedOverlayItem>();
		if (system == null)
			return;
		ArrayList<UPoint> points = system.getPoints();

		if (points == null)
			return;
		UPoint point = system.getStart();
		int size = system.getPoints().size();
		if (!isAfterAdd) {

			if (point == null && size > 0)
				point = system.getPoints().get(0);
			if (point == null) {
				GeoPoint position = ((MainActivity) getActivity())
						.getLastPosition();
				if (position != null)
					center = position;
			} else
				center = new GeoPoint(point.getLatitude(), point.getLongitude());
			mapView.getController().setCenter(center);
			mapView.invalidate();
		}
		ExtendedOverlayItem item;
		for (UPoint p : points) {
			item = new ExtendedOverlayItem(p.getTitle(), p.getSubtitle(),
					new GeoPoint(p.getLatitude(), p.getLongitude()),
					mapView.getContext());
			items.add(item);
		}
		return;
	}

	private class MapOverlay extends Overlay {
		public MapOverlay(Context context) {
			super(context);
		}

		@Override
		public boolean onLongPress(MotionEvent event, MapView mapView) {
			GeoPoint p = (GeoPoint) mapView.getProjection().fromPixels(
					(int) event.getX(), (int) event.getY());
			final UPoint point = new UPoint(p.getLatitude(), p.getLongitude(), false); //system.getByCoordinates(p);
				point.setTitle("Point #"+Integer.toString(system.getPoints().size()+1));
			new AlertDialog.Builder(view.getContext())
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setTitle("Please confirm")
					.setMessage(
							"Do you want to add point (" + point.getTitle()
									+ ") to your list?")
					.setNegativeButton("No", new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					}).setPositiveButton("Yes", new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							system.addPoint(point);
							loadPoints(true);
							reloadMapOverlays();
						}
					}).show();

			return super.onLongPress(event, mapView);
		}

		@Override
		protected void draw(Canvas canvas, MapView mapView, boolean shadow) {

		}
	}

}
