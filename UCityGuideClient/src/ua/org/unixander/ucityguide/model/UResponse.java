package ua.org.unixander.ucityguide.model;

import com.google.gson.Gson;

public class UResponse {
	public int status;
	public String message; 
	@Override
	public String toString() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}
}
