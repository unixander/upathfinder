package ua.org.unixander.ucityguide.model;

import com.google.gson.Gson;

public class URequest {
	public int code;
	public String command;
	public String[] args;
	
	public URequest(){}
	public URequest(int code){
		this.code = code;
	}
	
	@Override
	public String toString() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}
}
