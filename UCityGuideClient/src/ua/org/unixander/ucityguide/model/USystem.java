package ua.org.unixander.ucityguide.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.osmdroid.bonuspack.routing.MapQuestRoadManager;
import org.osmdroid.bonuspack.routing.Road;
import org.osmdroid.bonuspack.routing.RoadManager;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.PathOverlay;

import ua.org.unixander.ucityguide.utils.ExtendedGeocoderNominatim;
import ua.org.unixander.ucityguide.controllers.RouteManager;
import ua.org.unixander.ucityguide.model.UPoint;
import android.content.Context;
import android.location.Address;
import android.os.Bundle;

public class USystem {
	private static int MAX_RESULTS = 10, ERROR_OUT_OF_RANGE = 111, SUCCESS = 0;
	private ExtendedGeocoderNominatim geoCoder;
	@SuppressWarnings("unused")
	private Context context;
	private ArrayList<UPoint> points;
	private int startPoint, finishPoint;
	private String lastError;
	private double[][] weights;
	private static USystem instance = null;
	private boolean isLoop = false;
	private SearchType type;
	private RoadManager roadManager = new MapQuestRoadManager();
	private Road mRoad;
	private ArrayList<UPoint> respoints;
	private ArrayList<PathOverlay> overlays;
	private ServerCommands status;
	private UPoint defaultPoint = new UPoint(48.004738d, 37.800529d, false);

	public static void initSingletone(Context context) {
		if (instance != null)
			return;
		instance = new USystem(context);
	}

	public static USystem getInstance() {
		return instance;
	}

	private USystem(Context context) {

		this.context = context;
		this.points = new ArrayList<UPoint>();
		this.geoCoder = new ExtendedGeocoderNominatim(context,
				Locale.getDefault());
		this.startPoint = -1;
		this.finishPoint = -1;
	}

	public SearchType getType() {
		return type;
	}
	
	public ServerCommands getStatus(){
		return this.status;
	}
	
	public void setType(SearchType type) {
		this.type = type;
	}

	public UPoint getStart() {
		if (this.startPoint > -1)
			return points.get(this.startPoint);
		else
			return this.defaultPoint;
	}

	public UPoint getFinish() {
		if (this.finishPoint > -1)
			return points.get(this.finishPoint);
		else
			return null;
	}

	public boolean checkInPoints(UPoint point) {
		return points.indexOf(point) > -1;
	}

	public int setStart(int index) {
		if (this.startPoint >= 0) {
			points.get(startPoint).setStart(false);
		}
		if (index < 0 || index > points.size() - 1)
			return ERROR_OUT_OF_RANGE;
		this.startPoint = index;
		points.get(index).setStart(true);
		return SUCCESS;
	}

	public int setFinish(int index) {
		if (this.finishPoint >= 0) {
			points.get(finishPoint).setFinish(false);
		}
		if (index < 0 || index > points.size() - 1)
			return ERROR_OUT_OF_RANGE;
		this.finishPoint = index;
		points.get(index).setFinish(true);
		return SUCCESS;
	}

	public boolean checkStartAndFinish() {
		return this.finishPoint > -1 && this.startPoint > -1;
	}

	@SuppressWarnings("unchecked")
	public UPoint populatePoint(Address addr, UPoint point) {
		UPoint gpoint = point;
		Bundle bundle = addr.getExtras();
		HashMap<String, String> extended = (HashMap<String, String>) bundle
				.getSerializable("extended_info");
		String title = addr.getFeatureName();
		if (title == null) {
			title = "";
			if (extended.containsKey("road")) {
				title += extended.get("road") + " ";
			}
			if (extended.containsKey("house_number")) {
				title += extended.get("house_number") + " ";
			} else if (extended.containsKey("house")) {
				title += extended.get("house") + " ";
			}
			if (title == null || title.isEmpty())
				title = addr.getAddressLine(0) + "," + addr.getAddressLine(1);
		}

		String subtitle = "";
		if (addr.getAdminArea() != null)
			subtitle += addr.getAdminArea() + ", ";
		if (addr.getSubAdminArea() != null)
			subtitle += addr.getSubAdminArea() + ", ";
		if (addr.getCountryName() != null)
			subtitle += addr.getCountryName();
		gpoint.setTitle(title);
		gpoint.setSubtitle(subtitle);
		return gpoint;
	}

	public UPoint getByCoordinates(GeoPoint gpoint) {
		UPoint point = new UPoint();
		point.setLatitude(gpoint.getLatitude());
		point.setLongitude(gpoint.getLongitude());
		List<Address> addresses = null;
		try {
			addresses = geoCoder.getFromLocation(gpoint.getLatitude(),
					gpoint.getLongitude(), MAX_RESULTS);
			if (addresses.size() > 0) {
				for (Address addr : addresses) {
					point = populatePoint(addr, point);
					if (point.getTitle() != null || !point.getTitle().isEmpty())
						break;
				}
			}
		} catch (IOException e) {
			this.lastError = e.getLocalizedMessage();
		}
		if (point.getTitle() == null || point.getTitle().isEmpty())
			point.setTitle("Unnamed location");
		return point;
	}

	public ArrayList<UPoint> searchPoints(String query) {

		List<Address> addresses = null;
		ArrayList<UPoint> temps = new ArrayList<UPoint>();
		UPoint temp;
		try {
			addresses = geoCoder.getFromLocationName(query, MAX_RESULTS);
			if (addresses.size() > 0) {
				for (Address addr : addresses) {
					temp = new UPoint();
					temp = populatePoint(addr, temp);
					temp.setLatitude(addr.getLatitude());

					temp.setLongitude(addr.getLongitude());
					temps.add(temp);
				}
			}
		} catch (Exception e) {
			this.lastError = e.getLocalizedMessage();
		}
		return temps;
	}

	public void orderPoints() {
		UPoint finish = getFinish(), start = getStart();
		points.remove(finish);
		if (finishPoint == startPoint) {
			isLoop = true;
			points.add(0, finish);
		} else {
			isLoop = false;
			points.remove(start);
			points.add(0, start);
			points.add(finish);
		}
		recalculateIndexes();
	}

	public boolean calculateOptimal(Context mapContext) {
		this.orderPoints();
		return this.calculateCustomOptimal(mapContext);
	}

	public boolean calculateCustomOptimal(Context mapContext) {
		RouteManager manager = new RouteManager(mapContext);
		manager.setType(this.type);
		manager.setPoints(points);
		boolean result = manager.calculate();
		if (result) {
			this.respoints = manager.getRespoints();
			this.overlays = manager.getPathOverlay();
			if(this.type == SearchType.AUTO){
				this.overlays = new ArrayList<PathOverlay>();
				Road road = manager.getRoad();
				PathOverlay overlay;
				if(road != null){
					overlay = MapQuestRoadManager.buildRoadOverlay(road, mapContext);
					this.overlays.add(overlay);
				}
			}
		}
		this.lastError = manager.getServerMessage();
		this.status = manager.getStatus();
		return result;
	}

	public ArrayList<PathOverlay> getPaths() {
		return this.overlays;
	}

	public ArrayList<UPoint> getRespoints() {
		return this.respoints;
	}

	public Road getRoad() {
		return mRoad;
	}

	public void addPoint(UPoint point) {
		if (point != null)
			points.add(point);
	}

	public void deletePoint(int index) {
		if (index >= 0 && index < points.size()) {
			points.remove(index);
			recalculateIndexes();
		}
	}

	public ArrayList<UPoint> getPoints() {
		return points;
	}

	public String getLastError() {
		return lastError;
	}

	private void recalculateIndexes() {
		boolean start = false, finish = false;
		for (int i = 0; i < points.size(); i++) {
			UPoint point = points.get(i);
			if (point.isStart()) {
				this.startPoint = i;
				start = true;
			}
			if (point.isFinish()) {
				this.finishPoint = i;
				finish = true;
			}
			if (start && finish)
				break;
			if (!start)
				this.startPoint = -1;
			if (!finish)
				this.finishPoint = -1;
		}
	}

	public boolean setAfter(int position, int after) {
		if (position == after)
			return false;
		points.get(position).setPrev(points.get(after));
		return true;
	}

	public void clearAfter(int position) {
		points.get(position).setPrev(null);
	}
}
