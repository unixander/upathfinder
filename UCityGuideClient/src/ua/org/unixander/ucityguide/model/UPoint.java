package ua.org.unixander.ucityguide.model;

import org.osmdroid.util.GeoPoint;

public class UPoint {
	private double latitude;
	private double longitude;
	private boolean isStart = false, isFinish = false, isWayPoint = false, isStop = false;
	private String title, subtitle;
	private UPoint prev = null;
	
	public UPoint(){
		
	}
	
	public UPoint(double latitude, double longitude, boolean isWayPoint){
		this.latitude = latitude;
		this.longitude = longitude;
		this.isWayPoint = isWayPoint;
	}
	
	public boolean isStop(){
		return this.isStop;
	}
	
	public void setStop(boolean isStop){
		this.isStop = isStop;
	}
	
	public boolean isWayPoint() {
		return isWayPoint;
	}

	public void setWayPoint(boolean isWayPoint) {
		this.isWayPoint = isWayPoint;
	}

	public GeoPoint getGeoPoint() {
		return new GeoPoint(latitude, longitude);
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public boolean isStart() {
		return isStart;
	}

	public boolean isFinish() {
		return isFinish;
	}

	public String getTitle() {
		String result = title;
		return result;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public void setStart(boolean isStart) {
		this.isStart = isStart;
	}

	public void setFinish(boolean isFinish) {
		this.isFinish = isFinish;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRawSubtitle() {
		return subtitle;
	}
	
	public String getSubtitle() {
		if(this.isStop){
			return "Routes: "+subtitle;
		}
		return this.subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public UPoint getPrev() {
		return prev;
	}

	public void setPrev(UPoint prev) {
		this.prev = prev;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UPoint other = (UPoint) obj;
		if (this.title.equals(other.title)
				&& this.isStop == other.isStop
				&& this.isWayPoint == other.isWayPoint) {
			return true;
		}
		return false;
	}

}
