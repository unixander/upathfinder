package ua.org.unixander.ucityguide.dialogs;

import ua.org.unixander.ucityguide.R;
import ua.org.unixander.ucityguide.activities.MapActivity;
import ua.org.unixander.ucityguide.model.SearchType;
import ua.org.unixander.ucityguide.model.USystem;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Spinner;

public class SearchOptionsDialog extends DialogFragment {

	private Context context;
	private Spinner spinner;
	private USystem system;

	public static SearchOptionsDialog newInstance() {
		SearchOptionsDialog fragment = new SearchOptionsDialog();
		return fragment;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		this.context = getActivity().getApplicationContext();
		this.system = USystem.getInstance();
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view = inflater.inflate(R.layout.search_options_dialog, null);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		spinner = (Spinner) view.findViewById(R.id.searchType);
		builder.setView(view);
		builder.setPositiveButton("Search", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				int id = spinner.getSelectedItemPosition();
				SearchType type;
				switch (id) {
				case 0:
					type = SearchType.PUBLIC_TRANSIT;
					break;
				default:
					type = SearchType.AUTO;
				}
				system.setType(type);
				dialog.dismiss();
				Intent intent = new Intent(context, MapActivity.class);
				startActivity(intent);
			}
		});
		builder.setNegativeButton("Cancel", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		return builder.create();
	}

}
