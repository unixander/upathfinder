package ua.org.unixander.ucityguide.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import ua.org.unixander.ucityguide.R;
import ua.org.unixander.ucityguide.model.UPoint;

public class PointsListAdapter extends ArrayAdapter<String> {

	private Activity activity;
	private ArrayList<UPoint> points;

	static class ViewHolder {
		public TextView title, subtitle;
	}

	public PointsListAdapter(Activity activity, String[] titles,
			ArrayList<UPoint> points) {
		super(activity, R.layout.points_list_item, titles);
		this.points = points;
		this.activity = activity;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		UPoint point = points.get(position);
		ViewHolder holder;
		if (rowView == null) {
			LayoutInflater inflater = activity.getLayoutInflater();
			rowView = inflater.inflate(R.layout.points_list_item, null, true);
			holder = new ViewHolder();
			holder.title = (TextView) rowView.findViewById(R.id.title);
			holder.subtitle = (TextView) rowView.findViewById(R.id.subtitle);
			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}
		String title = Integer.toString(position+1)+") "+point.getTitle();
		if (point.isStart())
			title += " (Start)";
		if (point.isFinish())
			title += " (Finish)";
		if(point.getPrev()!=null){
			title+=" after #"+(points.indexOf(point.getPrev())+1);
		}
		holder.title.setText(title);
		holder.subtitle.setText(point.getSubtitle());

		return rowView;
	}
}
