package ua.org.unixander.ucityguide;

import java.io.IOException;

import ua.org.unixander.ucityguide.controllers.LocalDB;
import ua.org.unixander.ucityguide.controllers.ServerConnection;
import ua.org.unixander.ucityguide.model.USystem;
import android.app.Application;
import android.os.StrictMode;
import android.util.Log;

public class UCityGuideApp extends Application {

	private LocalDB localDB;
	
	public UCityGuideApp() {
		
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		initSingletons();
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

		StrictMode.setThreadPolicy(policy); 
	}

	/**
	 * Initialize singletons objects
	 */
	private void initSingletons() {
		ServerConnection.init(getApplicationContext());
		USystem.initSingletone(getApplicationContext());
		this.localDB = new LocalDB(getApplicationContext());
		try {
			this.localDB.createDataBase();
		} catch (IOException e) {
			Log.e("Application",e.getMessage());
		}
	}

	public LocalDB getLocalDB() {
		return localDB;
	}

	public void setLocalDB(LocalDB localDB) {
		this.localDB = localDB;
	}
	
	
}
