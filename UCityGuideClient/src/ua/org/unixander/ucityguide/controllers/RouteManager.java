package ua.org.unixander.ucityguide.controllers;

import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;

import org.osmdroid.bonuspack.routing.MapQuestRoadManager;
import org.osmdroid.bonuspack.routing.Road;
import org.osmdroid.bonuspack.routing.RoadManager;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.PathOverlay;

import ua.org.unixander.ucityguide.model.SearchType;
import ua.org.unixander.ucityguide.model.ServerCommands;
import ua.org.unixander.ucityguide.model.UPoint;
import ua.org.unixander.ucityguide.model.URequest;
import ua.org.unixander.ucityguide.model.UResponse;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class RouteManager {
	private Road mRoad = null;
	private Gson gson = new Gson();
	private ArrayList<UPoint> points, respoints;
	private ArrayList<PathOverlay> overlays;
	private ServerConnection connection;
	private Context context;
	private SearchType type;
	private String serverMessage;
	private ServerCommands status;
	private RoadManager roadManager = new MapQuestRoadManager();
	private PathOverlay[][] tempOverlays;
	
	public RouteManager(Context mapContext) {
		ServerConnection.init(mapContext.getApplicationContext());
		this.connection = ServerConnection.getInstance();
		this.context = mapContext;
	}

	public RouteManager(ArrayList<UPoint> points) {
		this.points = points;
	}
	
	public void setType(SearchType type){
		this.type = type;
	}
	
	public void setPoints(ArrayList<UPoint> points) {
		this.points = points;
	}

	public boolean calculate() {
		URequest request = new URequest();
		UResponse response = null;
		if (points == null || points.size() == 0) {
			return false;
		}
		switch(this.type){
		case PUBLIC_TRANSIT:
			request.command = ServerCommands.SEARCH_TRANSIT_ROUTE.toString();
			request.code = ServerCommands.SEARCH_TRANSIT_ROUTE.getCode();
			break;
		default:
			return this.calculateOptimalAutoRoute();
			//request.command = ServerCommands.SEARCH_AUTO_ROUTE.toString();
			//request.code = ServerCommands.SEARCH_AUTO_ROUTE.getCode();
		}
		
		String sPoints = gson.toJson(points);
		request.args = new String[] { sPoints };
		connection.open();
		response = gson.fromJson(connection.sendQuery(request.toString()),
				UResponse.class);
		if(response == null){
			this.status = null;
		}
		this.status = ServerCommands.getEnum(response.status);
		if(status == null){
			status = ServerCommands.SERVER_ERROR;
		}
		if (ServerCommands.OK_RESPONSE.getCode() == response.status) {
			Type token = new TypeToken<ArrayList<UPoint[]>>() {
			}.getType();
			ArrayList<UPoint[]> temp = gson.fromJson(response.message, token);
			generatePathOverlay(temp);
		} else {
			this.serverMessage = response.message;
		}
		connection.close();
		return response.status == ServerCommands.OK_RESPONSE.getCode();
	}
	
	public String getServerMessage(){
		return this.serverMessage;
	}
	
	public ServerCommands getStatus(){
		return this.status;
	}
	
	public Road getRoad() {
		return mRoad;
	}

	public ArrayList<PathOverlay> getPathOverlay() {
		return overlays;
	}

	public ArrayList<UPoint> getRespoints() {
		return respoints;
	}

	private void generatePathOverlay(ArrayList<UPoint[]> paths) {
		this.respoints = new ArrayList<UPoint>();
		this.overlays = new ArrayList<PathOverlay>();
		PathOverlay overlay;
		Log.i("Paths", "Paths quantity: "+Integer.toString(paths.size()));
		for (UPoint[] path : paths) {
			overlay = new PathOverlay(Color.RED, this.context);
			for(UPoint p: path){
				if(!p.isWayPoint()&&respoints.indexOf(p)==-1){
					respoints.add(p);
				}
				overlay.addPoint(p.getGeoPoint());
			}
			overlays.add(overlay);
		}
	}

	public int[] orderPoints(double[][] matrix){
		int[] sequence = null;
		URequest request = new URequest();
		UResponse response = null;
		String query = gson.toJson(matrix);
		request.command = ServerCommands.SEARCH_AUTO_ROUTE.toString();
		request.code = ServerCommands.SEARCH_AUTO_ROUTE.getCode();
		request.args = new String[] { query };
		connection.open();
		response = gson.fromJson(connection.sendQuery(request.toString()),
				UResponse.class);
		this.status = ServerCommands.getEnum(response.status);
		if(status == null){
			status = ServerCommands.SERVER_ERROR;
		}
		if (ServerCommands.OK_RESPONSE.getCode() == response.status) {
			Type token = new TypeToken<int[]>() {}.getType();
			sequence = gson.fromJson(response.message, token);
		} else {
			this.serverMessage = response.message;
		}
		connection.close();
		return sequence;
	}
	
	public boolean calculateOptimalAutoRoute(){
		int[] indexes;
		roadManager = new MapQuestRoadManager();
		roadManager.addRequestOption("key=Fmjtd%7Cluur21uyng%2Cb0%3Do5-90twgy");
		roadManager.addRequestOption("routeType=fastest");
		ArrayList<PathOverlay> route = new ArrayList<PathOverlay>();
		if(this.points.size()>2){
			double[][] matrix = this.getDistances();
			if(matrix.length > 3) {
				indexes = this.orderPoints(matrix);
			} else {
				indexes = new int[matrix.length];
				for(int i = 0; i < matrix.length; i++){
					indexes[i] = i;
				}
			}
			if (indexes == null || indexes.length == 0){
				this.status = ServerCommands.NOT_FOUND;
				this.serverMessage = "Route not found";
				return false;
			}
			ArrayList<UPoint> result = new ArrayList<UPoint>();
			for (int i = 0; i < indexes.length; i++) {
				result.add(points.get(indexes[i]));
				if(i != indexes.length - 1)
					route.add(tempOverlays[i][i+1]);
			}
			points = result;
		}
		ArrayList<GeoPoint> waypoints = new ArrayList<GeoPoint>();
		for (UPoint p : points) {
			waypoints.add(new GeoPoint(p.getLatitude(), p.getLongitude()));
		}
		this.mRoad = roadManager.getRoad(waypoints);
		if(this.mRoad.mStatus == Road.STATUS_DEFAULT) {
			mRoad = roadManager.getRoad(waypoints);
		}
		this.respoints = points;
		this.overlays = route;
		return mRoad != null && mRoad.mStatus == Road.STATUS_OK;
	}
	
	public double[][] getDistances() {
		int size = points.size();
		this.tempOverlays = new PathOverlay[size][size];
		double[][] distances = new double[size][size];
		ArrayList<GeoPoint> waypoints = null;
		UPoint start = null, finish = null;
		roadManager.addRequestOption("routeType=fastest");
		for (int i = 0; i < size; i++) {
			for (int j = i + 1; j < size; j++) {
				waypoints = new ArrayList<GeoPoint>();
				double length = 0;
				start = points.get(i);
				finish = points.get(j);
				waypoints.add(new GeoPoint(start.getLatitude(), start
						.getLongitude()));
				waypoints.add(new GeoPoint(finish.getLatitude(), finish
						.getLongitude()));
				Road road = roadManager.getRoad(waypoints);
				if (road.mStatus == Road.STATUS_OK)
					length = road.mLength;
				else
					length = 0;
				if(road != null)
					tempOverlays[i][j] = MapQuestRoadManager.buildRoadOverlay(road, context);
				distances[i][j] = distances[j][i] = length;
			}
		}
		return distances;
	}
}
