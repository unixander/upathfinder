package ua.org.unixander.ucityguide.controllers;

import java.util.ArrayList;
import java.util.Arrays;

import ua.org.unixander.ucityguide.fragments.*;
import android.app.Fragment;

public class FragmentsFactory {

	public static final int SETTINGS_FRAGMENT=1000,MAP_ROUTE_FRAGMENT=1001,MAP_VIEW_FRAGMENT=1002, POINTS_LIST_FRAGMENT = 1003,
			SEARCH_POINT_FRAGMENT = 1004;
	private static final ArrayList<Integer> codes = new ArrayList<Integer>(Arrays.asList(SETTINGS_FRAGMENT,
			MAP_VIEW_FRAGMENT, POINTS_LIST_FRAGMENT,SEARCH_POINT_FRAGMENT));
	private static final String[] titles = new String[]{"Settings","Map Select","Points List","Search Point"};
	public FragmentsFactory() {
		// TODO Auto-generated constructor stub
	}
	
	public static Fragment getFragmentByPosition(int position){
		int code = codes.get(position);
		return getFragment(code);
	}
	
	public static Fragment getFragment(int code){
		Fragment fragment = null;
		switch(code){
		case SETTINGS_FRAGMENT:
			fragment = new SettingsFragment();
			break;
		case MAP_VIEW_FRAGMENT:
			fragment = new MapViewFragment();
			break;
		case POINTS_LIST_FRAGMENT:
			fragment = new PointsListViewFragment();
			break;
		case SEARCH_POINT_FRAGMENT:
			fragment = new SearchPointFragment();
			break;
		}
		return fragment;
	}
	
	public static String[] getTitles(){
		return titles;
	}

}
