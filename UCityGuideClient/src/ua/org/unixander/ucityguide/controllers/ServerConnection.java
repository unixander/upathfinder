package ua.org.unixander.ucityguide.controllers;

import java.io.IOException;

import ua.org.unixander.ucityguide.model.ServerCommands;
import android.content.Context;
import android.util.Log;

public class ServerConnection {
	private NetworkConnection connection = null;
	private LocalDB dbHelper = null;
	private static ServerConnection instance;
	private String host;
	private int port;

	public static void init(Context context){
		if (instance != null)
			return;
		instance = new ServerConnection(context);
	}
	
	public static ServerConnection getInstance() {
		return instance;
	}
	
	public ServerConnection() {
		if (connection == null)
			connection = new NetworkConnection();
	}

	public ServerConnection(String host, int port) {
		if (connection == null)
			connection = new NetworkConnection(host, port);
	}

	public ServerConnection(Context context) {
		if (connection == null) {
			getSettingsFromDB(context);
			connection = new NetworkConnection(this.host, this.port);
		}
	}

	public String getError() {
		return connection.getLastError();
	}

	public void getSettingsFromDB(Context context) {
		dbHelper = new LocalDB(context);
		try {
			dbHelper.createDataBase();
			dbHelper.openDataBase();
			String h = dbHelper.getHost();
			int p = Integer.parseInt(dbHelper.getPort());
			dbHelper.close();
			if (!h.isEmpty())
				host = h;
			if (p != 0)
				port = p;
		} catch (IOException e) {
			Log.e("ServerConnection", e.getMessage());
		}
	}

	public boolean open() {
		return connection.connect();
	}

	public void close() {
		connection.disconnect();
	}

	public void reset() {
		connection.disconnect();
		connection.connect();
	}

	public boolean testConnection() {
		String answer = "";
		try {
			answer = connection.sendMessage(ServerCommands.CHECK_CONNECTION
					.toString());
		} catch (Exception e) {

		}
		return ServerCommands.OK_RESPONSE.equals(answer);
	}

	public String sendQuery(String query) {
		return connection.sendMessage(query);
	}

	public String[] sendQuery(String[] queries) {
		String[] response = new String[queries.length];
		for (int i = 0; i < queries.length; i++) {
			response[i] = connection.sendMessage(queries[i]);
		}
		return response;
	}
}
