package ua.org.unixander.UCityGuideServer;

import java.io.IOException;
import java.net.ServerSocket;

import ua.org.unixander.UCityGuideServer.log.ConsoleLog;


public class RunServer {

	private ServerSocket serverSocket = null;
	private ConsoleLog console = new ConsoleLog();
	
	public RunServer(String address, int port) {

		ServerSystem.init();
		boolean listening = true;

		try {
			serverSocket = new ServerSocket(port);
		} catch (IOException e) {
			console.Log("Could not listen on port: " + Integer.toString(port),
					ConsoleLog.WARNINGMSG);
			System.exit(-1);
		}
		console.Log("Server is running at " + serverSocket.getInetAddress().getHostAddress() 
				+ ":"+ Integer.toString(port) + " port",
				ConsoleLog.OTHERMSG);
		while (listening) {
			try {
				new ServerThread(serverSocket.accept()).start();
			} catch (Exception ex) {
				console.Log(ex.getMessage(), ConsoleLog.WARNINGMSG);
			}
			console.Log("New Connection", ConsoleLog.SUCCESSMSG);
		}

		try {
			serverSocket.close();
		} catch (IOException e) {
			console.Log(e.getMessage(), ConsoleLog.ERRORMSG);
		}

	}

	public static void main(String[] args) {
		String address = "localhost";
		int port = 4444;
		if (args.length == 2) {
			address = args[0];
			port = Integer.parseInt(args[1]);
		}
		new RunServer(address, port);
	}

}
