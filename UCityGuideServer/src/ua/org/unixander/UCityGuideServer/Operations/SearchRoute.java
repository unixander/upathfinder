package ua.org.unixander.UCityGuideServer.Operations;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import com.almworks.sqlite4java.SQLiteException;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import ua.org.unixander.UCityGuideServer.ServerSystem;
import ua.org.unixander.UCityGuideServer.entity.SearchType;
import ua.org.unixander.UCityGuideServer.entity.ServerCommands;
import ua.org.unixander.UCityGuideServer.entity.UPoint;
import ua.org.unixander.UCityGuideServer.log.ConsoleLog;
import ua.org.unixander.graph.Graph;
import ua.org.unixander.graph.Node;
import ua.org.unixander.graph.Position;
import ua.org.unixander.graph.algo.AStarAlgorithm;
import ua.org.unixander.graph.algo.AntColonyAlgorithm;
import ua.org.unixander.graph.algo.BranchAndBound;
import ua.org.unixander.graph.extendedalgo.AStarAlgorithmExt;
import ua.org.unixander.graph.extendedalgo.AntColonyAlgorithmExt;
import ua.org.unixander.graph.extendedalgo.applier.HighwayApply;
import ua.org.unixander.graph.extendedalgo.applier.PublicTransportApply;
import ua.org.unixander.graph.utils.SQLiteConnector;

public class SearchRoute {
	private int code;
	private Gson gson = new Gson();
	private ArrayList<UPoint[]> paths, tempPaths;
	private ArrayList<UPoint> points;
	private ServerSystem system;
	private Graph graph;
	private SQLiteConnector db;
	private BranchAndBound bnb = new BranchAndBound();
	private AntColonyAlgorithmExt ants;
	private SearchType type;
	private double[][] weights;

	public SearchRoute(String args, SearchType searchType) {
		this.init();
		this.type = searchType;
		Type atype = new TypeToken<ArrayList<UPoint>>() {
		}.getType();
		points = gson.fromJson(args, atype);
		switch (searchType) {
		case AUTO:
			AStarAlgorithm.setApplier(new HighwayApply(graph));
			break;
		case PUBLIC_TRANSIT:
			AStarAlgorithm.setApplier(new PublicTransportApply(graph));
		}
	}
	
	public SearchRoute(String args) {
		this.init();
		Type atype = new TypeToken<double[][]>() {
		}.getType();
		weights = gson.fromJson(args, atype);
	}
	
	private void init(){
		db = new SQLiteConnector();
		system = ServerSystem.getInstance();
		graph = system.getGraph();
	}

	public void calculate() throws SQLiteException {
		db.open();
		paths = new ArrayList<UPoint[]>();
		tempPaths = new ArrayList<UPoint[]>();
		ArrayList<Long> ids;
		Node source = null, target = null;
		UPoint src = null, dest = null;
		List<Node> tempNodes = new ArrayList<Node>();
		List<Node> path;
		int size = points.size();
		src = points.get(0);
		ids = db.getNearByIds(src.getLatitude(), src.getLongitude());
		source = chooseBestNodes(src, ids);
		tempPaths.add(new UPoint[] { src,
					new UPoint(source.getPosition().getLatitude(), source
					.getPosition().getLongitude(), true) });
		for (int i = 1; i < size; i++) {
			//src = points.get(i - 1);
			dest = points.get(i);
			//ids = db.getNearByIds(src.getLatitude(), src.getLongitude());
			//source = chooseBestNodes(src, ids);
			ids = db.getNearByIds(dest.getLatitude(), dest.getLongitude());
			target = chooseBestNodes(dest, ids);
			if (source == null || target == null) {
				this.code = ServerCommands.NOT_FOUND.getCode();
				return;
			}
			if (i == 1){
				tempNodes.add(source);
			}
			tempNodes.add(target);

			//tempPaths.add(new UPoint[] {
			//		src,
			//		new UPoint(source.getPosition().getLatitude(), source
			//				.getPosition().getLongitude(), true) });
			
			//TODO: Check type of search
			path = AStarAlgorithm.search(graph, source, target);
			if (path == null || path.size() == 0) {
				this.code = ServerCommands.NOT_FOUND.getCode();
				return;
			}
			paths.add(encodePoints(path));
			source = target;
			tempPaths.add(new UPoint[] {
					new UPoint(target.getPosition().getLatitude(), target
							.getPosition().getLongitude(), true), dest });
		}
		tempPaths.add(new UPoint[] {
				new UPoint(target.getPosition().getLatitude(), target
						.getPosition().getLongitude(), true), dest });

		// Route route = bnb.calculate(getWeights(tempNodes));
		
		int[] bestWay = this.calculateSequence(getWeights(tempNodes));
		if(bestWay == null){
			return;
		}
		List<Node> resultList = getNodes(bestWay, tempNodes);
		size = resultList.size();
		for (int i = 1; i < size; i++) {
			source = resultList.get(i - 1);
			target = resultList.get(i);
			//TODO: Check type of search
			path = AStarAlgorithm.search(graph, source, target);
			paths.add(tempPaths.get(i - 1));
			paths.add(encodePoints(path));
		}
		paths.add(tempPaths.get(size - 1));
		db.close();
		this.code = ServerCommands.OK_RESPONSE.getCode();
	}
	
	public int[] calculateSequence(double[][] weights){
		int[] bestWay = null;// route.getSequence();
		try {
			ants = new AntColonyAlgorithmExt(weights, 0, weights.length - 1);
			ants.calculate();
			bestWay = ants.getBestWay();
		} catch (IOException | InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		if (bestWay == null) {
			this.code = ServerCommands.NOT_FOUND.getCode();
		}
		return bestWay;
	}
	
	public String calculateAuto(){
		String response = new String();
		int[] sequence;
		if(this.weights != null){
			sequence = this.calculateSequence(this.weights);
			if(sequence != null){
				response = gson.toJson(sequence);
				this.code = ServerCommands.OK_RESPONSE.getCode();
			} else {
				this.code = ServerCommands.NOT_FOUND.getCode();
			}
		} else {
			this.code = ServerCommands.SERVER_ERROR.getCode();
		}
		return response;
	}

	public List<Node> getNodes(int[] sequence, List<Node> temp) {
		List<Node> list = new ArrayList<Node>();
		for (int i = 0; i < sequence.length; i++) {
			list.add(temp.get(sequence[i]));
		}
		return list;
	}

	public double[][] getWeights(List<Node> tempNodes) {
		int size = tempNodes.size();
		Node source, target;
		double[][] weights = new double[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (i == j) {
					weights[i][j] = 0;
				} else {
					source = tempNodes.get(i);
					target = tempNodes.get(j);
					AStarAlgorithm.search(graph, source, target);
					weights[i][j] = AStarAlgorithm.getDistance();
				}
				System.out.print(weights[i][j] + " ");
			}
			System.out.println();
		}
		return weights;
	}

	public int getCode() {
		return code;
	}

	public String getResponse() {
		db.close();
		if (paths != null && paths.size() != 0) {
			this.code = ServerCommands.OK_RESPONSE.getCode();
			return gson.toJson(paths);
		} else {
			this.code = ServerCommands.NOT_FOUND.getCode();
			return "Route not found";
		}
	}

	private Node chooseBestNodes(UPoint point, ArrayList<Long> ids) {
		Node result = null, destination, source;
		double min = Double.MAX_VALUE;
		source = new Node(point.getLatitude(), point.getLongitude());

		for (Long id : ids) {
			destination = graph.findNode(id);
			if (destination == null)
				continue;
			String highway = destination.getTag("highway");
			if(this.type == SearchType.AUTO && highway!=null && (highway.equals("pedestrian") 
					|| highway.equals("footway"))){
				continue;
			} else if (this.type == SearchType.PUBLIC_TRANSIT && !destination.checkTagKey("ref")){
				continue;
			}
			double distance = graph.calcDistance(source, destination);
			if (distance < min) {
				min = distance;
				result = destination;
			}
		}
		return result;
	}

	private UPoint[] encodePoints(List<Node> nodes) {
		UPoint[] ups = new UPoint[nodes.size()];
		UPoint temp;
		boolean isWayPoint = true;
		int i = 0;
		for (Node node : nodes) {
			Position position = node.getPosition();
			isWayPoint = !node.checkTagKey("bus");
			temp = new UPoint(position.getLatitude(), position.getLongitude(),
					isWayPoint);
			temp.setStop(!isWayPoint);
			String name = node.getTag("name");
			if (name != null && node.checkTagKey("bus")) {
				name = new String(name.getBytes(Charset.forName("UTF8")),
						Charset.forName("cp1251"));
			}
			temp.setTitle(name);
			if (!isWayPoint)
				temp.setSubtitle(node.getTag("ref"));
			ups[i] = temp;
			i++;
		}
		ups[0].setWayPoint(true);
		ups[ups.length - 1].setWayPoint(true);
		return ups;
	}

	@SuppressWarnings("unused")
	private double calcDistance(List<Node> path) {
		double distance = 0;
		Node src, dest;
		for (int i = 1; i < path.size(); i++) {
			src = path.get(i - 1);
			dest = path.get(i);
			distance += graph.calcDistance(src, dest);
		}
		return distance;
	}
}
