package ua.org.unixander.UCityGuideServer.entity;

public enum ServerCommands {
	CLOSE_CONNECTION(101),
	CONNECTION_CLOSED(102),
	CHECK_CONNECTION(199),
	UNKNOWN_COMMAND(401),
	NOT_FOUND(404),
	OK_RESPONSE(200),
	SEARCH_TRANSIT_ROUTE(300),
	SEARCH_AUTO_ROUTE(301),
	SERVER_ERROR(500);
	
	private Integer code;
	
	private ServerCommands(Integer code){
		this.code = code;
	}
	
	public Integer getCode(){
		return this.code;
	}
	
	@Override
	public String toString() {
		return this.code.toString();
	}
	
	public static ServerCommands getEnum(int value){
		for(ServerCommands command: ServerCommands.values()){
			if(command.getCode().equals(value)){
				return command;
			}
		}
		return UNKNOWN_COMMAND;
	}
}
