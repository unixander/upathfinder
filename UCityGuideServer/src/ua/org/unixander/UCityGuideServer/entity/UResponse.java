package ua.org.unixander.UCityGuideServer.entity;

import com.google.gson.Gson;

public class UResponse {
	public int status;
	public String message; 
	@Override
	public String toString() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}
}
