package ua.org.unixander.UCityGuideServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import ua.org.unixander.UCityGuideServer.log.ConsoleLog;

public class ServerThread extends Thread {
	private Socket socket = null;
	private ConsoleLog console = new ConsoleLog();

	public ServerThread(Socket socket) {
		super("ServerThread");
		this.socket = socket;
	}

	public void run() {
		PrintWriter out = null;
		BufferedReader in = null;
		try {
			console.Log("Client " + socket.getRemoteSocketAddress().toString()
					+ " is connected", ConsoleLog.SUCCESSMSG);
			out = new PrintWriter(socket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));

			String inputLine, outputLine;
			ServerProtocol protocol = new ServerProtocol();
			while ((inputLine = in.readLine()) != null) {
				outputLine = protocol.processInput(inputLine);
				if (outputLine == null)
					outputLine = "null";
				out.println(outputLine);
				if (outputLine.equals("CLOSE_CONNECTION"))
					break;
			}

		} catch (IOException e) {
			console.Log(e.getMessage(), ConsoleLog.WARNINGMSG);
		} finally {
			try {
				if (out != null)
					out.close();
				if (in != null)
					in.close();
				if (socket != null)
					socket.close();
			} catch (IOException e) {

			}
			console.Log("Client " + socket.getRemoteSocketAddress().toString()
					+ " disconnected", ConsoleLog.OTHERMSG);
		}
	}
}
