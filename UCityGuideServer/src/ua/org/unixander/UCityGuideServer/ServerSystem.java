package ua.org.unixander.UCityGuideServer;

import ua.org.unixander.graph.*;
import ua.org.unixander.graph.utils.*;

public class ServerSystem {
	private static ServerSystem instance;
	private Graph graph;
	
	public static void init(){
		if(instance==null){
			instance = new ServerSystem();
		}
	}
	
	public static ServerSystem getInstance(){
		return instance;
	}
	
	private ServerSystem(){
		GraphBuilder builder = new GraphBuilder();
		//builder.startId = 1280287429;//1067363212;//1067363212;1326181881
		//SQLiteConnector.DB_PATH = "\\db\\routes.sqlite";
		builder.loadGraphFromDB();
		this.graph = builder.getGraph();
	}

	public Graph getGraph() {
		return graph;
	}

	public void setGraph(Graph graph) {
		this.graph = graph;
	}
	
	
}
