package ua.org.unixander.UCityGuideServer;

import com.google.gson.Gson;

import ua.org.unixander.UCityGuideServer.Operations.SearchRoute;
import ua.org.unixander.UCityGuideServer.entity.SearchType;
import ua.org.unixander.UCityGuideServer.entity.ServerCommands;
import ua.org.unixander.UCityGuideServer.entity.URequest;
import ua.org.unixander.UCityGuideServer.entity.UResponse;
import ua.org.unixander.UCityGuideServer.log.ConsoleLog;

public class ServerProtocol {
	private static ConsoleLog console = new ConsoleLog();
	private Gson gson = new Gson();

	public ServerProtocol() {

	}

	public String processInput(String theInput) {
		URequest request = null;
		UResponse response = new UResponse();
		String answer = "";
		SearchRoute sRoute;
		SearchType type = SearchType.AUTO;
		console.Log(theInput, ConsoleLog.DEBUG);
		try {
			if (theInput != null && !theInput.isEmpty()) {
				request = gson.fromJson(theInput, URequest.class);
				ServerCommands command = ServerCommands.getEnum(request.code);
				switch (command) {
				case CHECK_CONNECTION:
					response.status = ServerCommands.OK_RESPONSE.getCode();
					break;
				case CLOSE_CONNECTION:
					response.status = ServerCommands.CONNECTION_CLOSED
							.getCode();
					break;
				case SEARCH_AUTO_ROUTE:
					sRoute = new SearchRoute(request.args[0]);
					response.message = sRoute.calculateAuto();
					response.status = sRoute.getCode();
					break;
				case SEARCH_TRANSIT_ROUTE:
					type = SearchType.PUBLIC_TRANSIT;
					sRoute = new SearchRoute(request.args[0], type);
					sRoute.calculate();
					response.status = sRoute.getCode();
					response.message = sRoute.getResponse();
					break;
				default:
					response.status = ServerCommands.UNKNOWN_COMMAND.getCode();
					break;
				}
			}
		} catch (Exception e) {
			response.status = ServerCommands.SERVER_ERROR.getCode();
			response.message = e.getMessage();
			e.printStackTrace();
			console.Log("ServerProtocol: "+answer, ConsoleLog.ERRORMSG);
		}
		answer = response.toString();
		if (answer == null)
			answer = "null";
		console.Log(answer, ConsoleLog.DEBUG);
		return answer;
	}
}
